const { baseUrl } = document.currentScript.dataset;
$(function () {
    /**
     * 獲取對象，並轉換成jQuery實例返回。
     * @param {*} target 
     * @returns 
     */
    function _getTarget(target) {
        // 如果傳入的對象為字符串或者原生的html元素，則加上jQuery;
        if (typeof target === 'string' || target instanceof HTMLElement) {
            return $(target);
        } 
        // 如果目標為jQuery實例，則直些返回。
        if (target instanceof jQuery) {
            return target;
        }

        return null;
    }

    /**
     * 右上角的提示框
     * @param {String} type 
     * @param {String} msg
     * @param {Number} wait
     */
    function _showMsg(type, msg = '', wait = 3) {
        // 獲取提示框範圍
        let $myAlert = $('body #myAlert');
        // 如果提示框範圍並不存在於頁面之中，則進行創建並加入到body裏面。
        if (!$myAlert.length) {
            $myAlert = $('<div id="myAlert" class="my-alert"></div>');
            $('body').append($myAlert);
        }
        // 提示框
        const $alert = $(`
            <div class="alert alert-${type} fade alert-dismissible" role="alert">
                <div data-bs-dismiss="alert">${msg}</div>
                <button type="button" class="btn-close mt-1" data-bs-dismiss="alert" aria-label="Close"></button>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="--speed: ${wait}s;"></div>
                </div>
            </div>
        `);
        // 將提示框加入到提示框範圍的最後一位。
        $myAlert.prepend($alert);
        // 用於效果延遲，如果沒有延遲的話，無法正確顯示出動畫效果。
        setTimeout(() => {
            $alert.addClass('show');
            
            // 顯示之後於多少秒關閉此提示框。
            wait && setTimeout(() => {
                bootstrap.Alert.getOrCreateInstance($alert.get(0)).close();
            }, wait * 1000);
        }, 50);
    }
    
    // 封裝 ajax 請求為 promise
    function _ajax(type, url, data) {
        url = _toUrl(url);

        return new Promise((resolve) => {
            $.ajax({
                type,
                url,
                data: JSON.stringify(data),
                success: function (data) {
                    resolve(data);
                },
                contentType: 'application/json',
                dataType: 'json',
            }).fail(function (e) {
                resolve(e.responseJSON);
            });
        });
    }

    /**
     * 畫面請求數據時的過渡效果
     * @param {*} target 
     * @returns 
     */
    function _spinner(target) {
        const templateHtml = `
            <span class="spinner-grow" role="status" aria-hidden="true"></span>
        `;

        const $target = _getTarget(target || 'body');

        if ($target.is('button, [type=button]')) {
            let $spinner = $(templateHtml);
            $spinner.addClass('spinner-grow-sm me-2');
            $target.prepend($spinner);
            return;
        }
    }

    /**
     * 關閉過度效果
     * @param {*} target 
     * @returns 
     */
    function _closeSpinner(target) {
        let $target = _getTarget(target || 'body');

        if ($target.is('button, [type=button]')) {
            $target.find('.spinner-grow').remove();
            return;
        }
    }
    /**
     * 獲取目標範圍的輸入資料。
     * @param {*} target 目標
     * @param {*} onlyChange 是否只獲取有修改的(需搭配initData一同使用)
     * @param {*} notEmpty 是否過慮空值
     * @returns 
     */
    function _getData(target, onlyChange, notEmpty = false) {
        const $target = _getTarget(target);
        if (!$target) {
            return null;
        }

        const data = {};

        $target.find(':input[name]:not(button, [type=button], [type=submit])').each(function() {
            if (onlyChange === true && $(this).attr('data-ori-value') == $(this).val()) {
                return;
            }

            if (notEmpty === true && !$(this).val()) {
                return;
            }

            data[this.name] = $(this).val();
        });

        return data;
    }

    /**
     * 獲取目標範圍的修改資料。
     * @param {*} target 
     * @param {Array<String>} keys 不倫是否修改，都會獲取的欄位。
     * @returns 
     */
    function _getChangeData(target, keys = []) {
        const $target = _getTarget(target);
        if (!$target) {
            return null;
        }

        const data = {};

        $target.find(':input[name]:not(button, [type=button], [type=submit])').each(function() {
            if (!(keys || []).includes(this.name) && $(this).attr('data-ori-value') == $(this).val()) {
                return;
            }

            data[this.name] = $(this).val();
        });

        return data;
    }

    /**
     * 清除目標範圍的輸入資料
     * @param {*} target 
     * @returns 
     */
    function _clearData(target) {
        let $target = _getTarget(target);
        if (!$target) {
            return null;
        }

        let data = {};

        $target.find(':input[name]:not(button, [type=button], [type=submit])').each(function() {
            if ($(this).is('select')) {
                $(this).val(function() {
                    return $(this).find(':first').val();
                });
                return;
            }
            if ($(this).is('[type=check], [type=radio]')) {
                $(this).prop('checked', false);
                return;
            }

            $(this).val(null);
        });

        return data;
    }

    /**
     * confirm
     * @param {*} message 
     * @returns 
     */
    function _confirm(message) {
        return new Promise(resolve => {
            bootbox.confirm({
                message,
                locale: navigator.language,
                callback: function (result) {
                    resolve(result);
                }
            });
        })
    }

    /**
     * 初始化目標範圍的所有輸入筐，用於檢查是否有資料異動。
     * @param {*} target 
     * @param {*} data 
     */
    function _initData(target, data = {}) {
        const $target = _getTarget(target);

        for (const [ field, value ] of Object.entries(data)) {
            $target.find(`[name="${field}"]`).val(value);
            $target.find(`[name="${field}"]`).attr('data-ori-value', value);
        }
    }

    /**
     * 欄位檢核
     * @param {*} target 
     * @returns 
     */
    function _checkValidity(target) {
        const $target = _getTarget(target);

        let result = $target.get(0).checkValidity();
        
        _clearValidity(target);

        if ($target.is('.needs-validation')) {
            $target.find(':input').each(function() {
                if (!this.validity.valid) {
                    if (this.validity.valueMissing) {
                        $(this).addClass('is-invalid');
                        $(this).siblings('.invalid-feedback').text($(this).attr('data-require-msg'));
                    } else if (this.validity.patternMismatch) {
                        $(this).addClass('is-invalid');
                        $(this).siblings('.invalid-feedback').text($(this).attr('data-pattern-msg'));
                    } else if (this.validity.customError) {
                        $(this).addClass('is-invalid');
                        $(this).siblings('.invalid-feedback').text($(this).attr('data-custom-msg'));
                    } else if (this.validity.tooShort) {
                        $(this).addClass('is-invalid');
                        $(this).siblings('.invalid-feedback').text($(this).attr('data-too-short-msg'));
                    }
                } else if ($(this).is('[data-min-length]') && (+$(this).attr('data-min-length')) > $(this).val().length) {
                    $(this).addClass('is-invalid');
                    $(this).siblings('.invalid-feedback').text($(this).attr('data-too-short-msg'));
                    result = false;
                }
            });
        }

        return result;
    }

    /**
     * 清除欄位檢核的訊息
     * @param {*} target 
     */
    function _clearValidity(target) {
        const $target = _getTarget(target);
        $target.find('.is-invalid').removeClass('is-invalid');
    }

    /**
     * 將欄位恢復為初始值
     * @param {*} target 
     * @returns 
     */
    function _resetValueToOri(target) {
        const $target = _getTarget(target);
        if ($target.is(':input')) {
            $target.val(function() {
                return $(this).attr('data-ori-value') || '';
            });
            return;
        }

        $target.find(':input[data-ori-value]').val(function() {
            return $(this).attr('data-ori-value') || '';
        });
    }

    /**
     * 檔案下載
     * @param {*} url 
     * @returns 
     */
    function _download(url) {
        url = _toUrl(url);

        return new Promise((resolve) => {
            const xhr = new XMLHttpRequest()
            xhr.onreadystatechange = function () {
                if (this.readyState === 4) {
                    if (this.status === 200) {
                        let filename = "";
                        const disposition = xhr.getResponseHeader('Content-Disposition');
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            const matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) { 
                                filename = matches[1].replace(/['"]/g, '');
                            }
                        }
    
                        saveAs(this.response, decodeURI(filename));
                        resolve({ success: true });
                    } else {
                        const reader = new FileReader();
                        reader.onload = function() {
                            resolve(JSON.parse(reader.result));
                        }
                        reader.readAsText(this.response);
                    }
                }
            }
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        });
    }

    /**
     * 前往對應網址
     * @param {*} url 
     * @param {*} isReplace 
     */
    function _goto(url, isReplace = false) {
        url = _toUrl(url);

        if (isReplace) {
            window.location.replace(url);
        } else {
            window.location.href = url;
        }
    }

    /**
     * 上傳檔案
     * @param {*} url 
     * @param {*} file 
     * @returns 
     */
    function _upload(url, file) {
        url = _toUrl(url);

        const formData = new FormData();
        formData.set('file', file);

        return new Promise((resolve) => {
            $.ajax({
                type: 'POST',
                url,
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    resolve(data);
                },
            }).fail(function (e) {
                resolve(e.responseJSON);
            });
        });
    }

    /**
     * 為確保網址的正確性，在此判斷是否有加上前綴http，
     * 如果沒有則需要自行帶入當前系統的基礎網址。
     * @param {*} url 
     * @returns 
     */
    function _toUrl(url) {
        if (url && !url.startsWith('http')) {
            url = baseUrl + url;
        }

        return url;
    }

    /**
     * 複製文字
     * @param {*} text 
     */
    function _copyText(text) {

        const el = document.createElement('textarea');
        el.value = text;

        document.body.appendChild(el);

        el.select();
        el.focus();

        document.execCommand('copy');
        document.body.removeChild(el);
    }

    /**
     * 等待/睡眠
     * @param {*} time 毫秒
     * @returns 
     */
    function _sleep(time) {
        return new Promise(resolve => {
            setTimeout(() => resolve(), time);
        });
    }

    window.util = {
        ...window.util,
        confirm: msg => _confirm(msg),
        form: {
            getData: (target, onlyChange, notEmpty) => _getData(target, onlyChange, notEmpty),
            getChangeData: (target, keys) => _getChangeData(target, keys),
            clearData: target => _clearData(target),
            initData: (target, data) => _initData(target, data),
            checkValidity: target => _checkValidity(target),
            clearValidity: target => _clearValidity(target),
            resetValueToOri: target => _resetValueToOri(target),
        },
        post: (url, data) => _ajax('POST', url, data),
        get: (url, data) => _ajax('GET', url, data),
        put: (url, data) => _ajax('PUT', url, data),
        delete: (url, data) => _ajax('DELETE', url, data),
        upload: (url, file) => _upload(url, file),
        download: (url) => _download(url),
        showSuccess: (msg, wait) => { _showMsg('success', msg, wait) },
        showError: (msg, wait) => { _showMsg('danger', msg, wait) },
        showWarning: (msg, wait) => { _showMsg('warning', msg, wait) },
        spinner: {
            show: target => _spinner(target),
            close: target => _closeSpinner(target),
        },
        location: {
            href: url => _goto(url),
            replace: url => _goto(url, true),
        },
        toUrl: url => _toUrl(url),
        copyText: text => _copyText(text),
        sleep: time => _sleep(time),
    }
});
