$(function () {
    $("#loginForm").on("submit", login);
    $('#registerForm').on('submit', register);
    $('#registerForm [name="password"], #registerForm [name="password_confirmation"]').on('change', checkConfirmPassword);

    async function register(e) {
        e.preventDefault();

        if (!util.form.checkValidity(this)) {
            return;
        }

        let $btnSubmit = $(this).find('[type=submit]');
        
        util.spinner.show($btnSubmit);
        
        $btnSubmit.prop('disabled', true);

        let data = util.form.getData(this);

        let result = await util.post('/api/register', data);

        util.spinner.close($btnSubmit);
        $btnSubmit.prop('disabled', false);

        if (!result.success) {
            return util.showError(result.msg);
        }

        util.showSuccess(result.msg);
        $("#login-button").trigger('click');
        $('#loginForm [name=account]').val(data.account);
    }

    function checkConfirmPassword() {
        const newPassword = $('#registerForm [name="password"]').val();
        const confirmPassword = $('#registerForm [name="password_confirmation"]').val();
        
        if (newPassword === confirmPassword) {
            $('#registerForm [name="password_confirmation"]').get(0).setCustomValidity('');
        } else {
            $('#registerForm [name="password_confirmation"]').get(0).setCustomValidity('error');
        }
    }

    async function login(e) {
        e.preventDefault();

        if (!util.form.checkValidity(this)) {
            return;
        }

        util.spinner.show("#btnLogin");
        $("#btnLogin").prop("disabled", true);


        const data = util.form.getData(this);
        let result = await util.post("/api/login", data);

        util.spinner.close("#btnLogin");
        $("#btnLogin").prop("disabled", false);

        if (!result.success) {
            return util.showError(result.msg);
        }

        util.location.replace('/');
    }


    $('#signup-button').click(function() {
        $('#user_options-forms').removeClass('bounceRight');
        $('#user_options-forms').addClass('bounceLeft');
        util.form.clearData($('#user_options-forms #loginForm'));
        util.form.clearValidity($('#user_options-forms #loginForm'));
        
    });

    $('#login-button').click(function() {
        $('#user_options-forms').addClass('bounceRight');
        $('#user_options-forms').removeClass('bounceLeft');
        util.form.clearData($('#user_options-forms #registerForm'));
        util.form.clearValidity($('#user_options-forms #registerForm'));
    });
});
