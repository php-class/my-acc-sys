$(function() {
    $('#profileForm').on('submit', modify);

    $('.msg .alert').addClass('show');

    util.get('/api/profile').then(result => {
        
        if (!result.success) {
            return util.showError(result.msg);
        }
        
        util.form.initData('#profileForm', result.data);
        showAvatar(result.data.avatar);
    });


    async function modify(e) {
        e.preventDefault();

        let data = util.form.getData(this, true);
        
        if ($.isEmptyObject(data)) {
            return;
        }

        util.spinner.show($(this).find('[type=submit]'));
        $(this).find(':input').prop('disabled', true);

        let result = await util.put('/api/profile', data);
        
        util.spinner.close($(this).find('[type=submit]'));
        $(this).find(':input').prop('disabled', false);

        if (!result.success) {
            return util.showError(result.msg);
        }
        util.form.initData('#profileForm', data);
        $(this).find(':focus').blur();
        return util.showSuccess(result.msg);
    }

    $('#btnPreview').click(function() {
        $('input#file').trigger('click');
    });

    $('input#file').on('change', function() {
        if (!this.files || !this.files[0]) {
            return;
        }

        $('#modalPreview').modal('show');

        const [file] = this.files;
        const url = URL.createObjectURL(file);

        let count = 2;
        $('#preview').on('load', function() {
            if (--count === 0) {
                URL.revokeObjectURL(url);
            }
        });

        $('#previewBg').on('load', function() {
            if (--count === 0) {
                URL.revokeObjectURL(url);
            }
        });

        $('#preview').prop('src', url);
        $('#previewBg').prop('src', url);
    });

    $('#modalPreview').on('hidden.bs.modal', function() {
        $('#preview').prop('src', '');
        $('#previewBg').prop('src', '');
        $('input#file').val(null);
    });

    $('#btnUpload').click(async function() {
        const files = $('input#file').get(0).files;

        if (!files || !files[0]) {
            return;
        }

        let result = await util.upload('/api/file', files[0]);
        
        if (!result.success) {
            return util.showError(result.msg);
        }
        if ($('#profileForm [name=avatar]').val()) {
            util.delete(`/api/file/${$('#profileForm [name=avatar]').val()}`);
        }
        util.put('/api/profile', { avatar: result.data.file_id });

        showAvatar(result.data.file_id);

        $('#modalPreview').modal('hide');
    });

    function showAvatar(id) {
        if (!id) {
            return;
        }

        $('#avatar').one('load', function() {
            $(this).prev('i').hide();
            $(this).show();
        });

        $('#profileForm [name=avatar]').val(id);
        $('#avatar').prop('src', util.toUrl(`/api/file/${id}`));
    }
});