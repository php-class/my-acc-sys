$(function () {
    const $table = $("#resultTable").bootstrapTable({
        idField: "id",
        paginationVAlign: "top",
        locale: navigator.language,
        paginationLoop: false,
        pagination: true,
        sidePagination: "server",
        pageSize: 10,
        pageList: [5, 10, 25],
        queryParams,
        responseHandler,
        columns: [
            {
                field: "role",
                formatter: function(value, row, index) {
                    if (value === 'ADMIN') {
                        return this.roleAdmin;
                    }

                    return this.roleGeneral;
                },
            },
            {
                field: "account",
            },
            {
                field: "is_active",
                formatter: function(value, row, index) {
                    if (value == 1) {
                        return `<span class="light-green">${this.yes || $('#resultTable').attr('data-yes')}</span>`
                    }

                    return `<span class="light-red">${this.no || $('#resultTable').attr('data-no')}</span>`
                },
            },
            {
                field: "last_login_at",
                formatter: function(value, row, index) {
                    return value && moment(value).format('YYYY/MM/DD HH:mm:ss');
                },
            },
            {
                field: "operate",
                formatter: operateFormatter,
                events: {
                    'click .edit': openModalEditData,
                    'click .generate-password': generatePassword,
                }
            },
        ],
    });

    // 查詢提交
    $("#queryForm").on("submit", function (e) {
        e.preventDefault();
        $table.bootstrapTable("refresh", { pageNumber: 1 });
    });

    // 重置查詢條件
    $("#reset").click(function () {
        util.form.clearData("#queryForm");
        $("#queryForm [type=date]").prop('min', null);
        $("#queryForm [type=date]").prop('max', null);
        $table.bootstrapTable("refresh", { pageNumber: 1 });
    });

    // 當日期設定時，確保不起日不會超過迄日設定
    $("[name=date_fm], [name=date_to]").change(function () {
        if (this.name === "date_fm") {
            $("[name=date_to]").get(0).min = this.value;
        } else {
            $("[name=date_fm]").get(0).max = this.value;
        }
    });

    $('.modal').on('hidden.bs.modal', function() {
        util.form.clearData(this);
        util.form.clearValidity($(this).find('form'));
    });

    function queryParams(params) {
        let { limit, offset } = params;
        let data = util.form.getData("#queryForm", void 0, true);

        return { ...data, limit, offset };
    }

    function responseHandler(res) {
        return {
            rows: res.data,
            total: res.total,
        };
    }

    function operateFormatter(value, row, index) {
        const actions = [];

        if (row.account === 'admin') {
            return;
        }

        if ($('#resultTable').data('currentUserId') == row.id) {
            return;
        }

        actions.push(`
            <button type="button" class="btn btn-success edit" data-id="${row.id}">
                <i class="bi bi-pencil-square"></i>
                ${this.editTitle || ''}
            </button>
        `);

        return `
            <div>
                ${actions.join('')}
            </div>
        `;
    }

    // 打開編輯視窗
    async function openModalEditData(e, value, row, index) {
        $('#modalEdit').find('[name="id"]').val(row.id);
        
        let result = await util.get(`/api/admin/member/${row.id}`);

        if (!result.success) {
            return util.showError(result.msg);
        }

        util.form.initData($('#modalEdit form'), result.data);

        $('#modalEdit').modal('show');
    }

    $('#editForm').on('submit', async function(e) {
        e.preventDefault();

        if (!util.form.checkValidity(this)) {
            return;
        }

        let data = util.form.getData(this, true);

        if ($.isEmptyObject(data)) {
            return;
        }

        data.id = $(this).find('[name=id]').val();

        util.spinner.show($(this).find('[type=submit]'));
        $(this).find(':input').prop('disabled', true);

        let result = await util.put(`/api/admin/member/${data.id}`, data);
        
        util.spinner.close($(this).find('[type=submit]'));
        $(this).find(':input').prop('disabled', false);

        if (!result.success) {
            return util.showError(result.msg);
        }

        $(this).closest('.modal').modal('hide');
        util.showSuccess(result.msg);
        $table.bootstrapTable("refresh");
    });

    $("#btnDelete").click(async function() {
        
        let rows = $table.bootstrapTable('getSelections').map(({ id }) => id);
        if (!rows.length) {
            return;
        }

        if ($(this).is('[data-delete-confirm-msg]')) {
            let isConfirm = await util.confirm($(this).attr('data-delete-confirm-msg'));
            if (!isConfirm) {
                return;
            }
        }

        let result = await util.delete('/api/admin/member', { rows });

        if (!result.success) {
            return util.showError(result.msg);
        }

        $table.bootstrapTable("refresh", { pageNumber: 1 });
        return util.showSuccess(result.msg);
    });

    async function generatePassword(e, value, row, index) {
        const result = await util.put(`/api/admin/member/generatePassword/${row.id}`);

        if (!result.success) {
            return util.showError(result.msg);
        }

        $('#newPassword').text(result.data.password);
        $('#modalNewPassword').modal('show');
    }

    $('#modalNewPassword').on('hidden.bs.modal', function() {
        $('#newPassword').text(null);
    });

    $('#btnCopy').click(async function() {
        if ($(this).prop('copy')) {
            return;
        }

        const html = $(this).html();
        $(this).prop('copy', true);
        util.copyText($('#newPassword').text());
        $(this).html('<i class="bi bi-check2-square"></i>');
        
        await util.sleep(2000);
        $(this).html(html);
        $(this).prop('copy', false);
    });
});
