$(function () {
    Chart.register(ChartDataLabels);

    const pieOptions = {
        plugins: {
            title: {
                display: true,
            },
            legend: {
                position: "bottom",
            },
            datalabels: {
                formatter: (value, ctx) => {
                    let sum = 0;
                    let dataArr = ctx.chart.data.datasets[0].data;
                    dataArr.map((data) => {
                        sum += +data;
                    });
                    let percentage = ((+value * 100) / sum).toFixed(2) + "%";
                    return percentage;
                },
                color: "#fff",
            },
        },
    };

    const statisticsChart = initStatisticsChart();

    util.get("/api/expenditure/statistics").then((result) => {
        if (!result.success) {
            return util.showError(result.msg);
        }

        const { chart, data, datasets } = statisticsChart;
        const dataset = datasets['TOTAL'];
        dataset['amount'] = {...data};

        for (const { amount, date, type } of result.data) {
            if (!datasets[type]) {
                datasets[type] = {
                    label: type,
                    amount: {...data},
                };
            }
            if (typeof data[date] !== 'undefined') {
                dataset['amount'][date] += +amount;
                datasets[type]['amount'][date] += +amount;
            }
        }

        for (const type in datasets) {
            datasets[type]['data'] = Object.values(datasets[type]['amount']);
        }

        chart.data.datasets.push(...Object.values(datasets));
        chart.update();
    });

    util.get("/api/expenditure/statistics/type").then((result) => {
        if (!result.success) {
            return util.showError(result.msg);
        }

        const labels = [];
        const data = [];

        for (let { name, count } of result.data) {
            labels.push(name);
            data.push(count);
        }

        const $ctx = $("#statisticsByType");

        new Chart($ctx.get(0), {
            type: "pie",
            data: {
                labels,
                datasets: [
                    {
                        label: $ctx.attr("data-label"),
                        data,
                        hoverOffset: 4,
                    },
                ],
            },
            options: {
                ...pieOptions,
                plugins: {
                    ...pieOptions.plugins,
                    title: {
                        display: true,
                        text: $ctx.attr("data-title"),
                    }
                }
            },
        });
    });

    util.get("/api/expenditure/statistics/payment").then((result) => {
        if (!result.success) {
            return util.showError(result.msg);
        }

        const labels = [];
        const data = [];

        for (let { name, count } of result.data) {
            labels.push(name);
            data.push(count);
        }

        const $ctx = $("#statisticsByPaymentMethod");

        new Chart($ctx.get(0), {
            type: "pie",
            data: {
                labels,
                datasets: [
                    {
                        label: $ctx.attr("data-label"),
                        data,
                        hoverOffset: 4,
                    },
                ],
            },
            options: {
                ...pieOptions,
                plugins: {
                    ...pieOptions.plugins,
                    title: {
                        display: true,
                        text: $ctx.attr("data-title"),
                    }
                }
            },
        });
    });

    function initStatisticsChart() {
        const $ctx = $("#statistics");
        const labels = {};
        const data = {};
        const now = moment();
        const last = moment().subtract(1, 'M');
        const diff = now.diff(last, 'd');

        labels[last.format('YYYY-MM-DD')] = last.format('MM/DD');
        data[last.format('YYYY-MM-DD')] = 0;

        for (let idx = 0; idx < diff; idx++) {
            let day = last.add(1, 'd');
            labels[day.format('YYYY-MM-DD')] = day.format('MM/DD');
            data[day.format('YYYY-MM-DD')] = 0;
        }

        const datasets = {
            'TOTAL': {
                label: $ctx.attr('data-label'),
                data: Object.values({...data}),
            },
        };

        let statisticsChart = new Chart($ctx.get(0), {
            type: "line",
            data: {
                labels: Object.values(labels),
                datasets: Object.values(datasets),
            },
            options: {
                maintainAspectRatio: false,
                responsive: true,
                plugins: {
                    title: {
                        display: true,
                        text: $ctx.attr('data-title'),
                    },
                    colors: {
                        forceOverride: true
                    }
                },
                interaction: {
                    intersect: false,
                },
                scales: {
                    x: {
                        display: true,
                        title: {
                            display: true,
                            text: $ctx.attr('data-x-label'),
                        },
                    },
                    y: {
                        display: true,
                        title: {
                            display: true,
                            text: $ctx.attr('data-y-label'),
                        },
                        suggestedMin: 0,
                    },
                },
            },
        });

        return { chart: statisticsChart, labels, data, datasets };
    }
});
