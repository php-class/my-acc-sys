$(function() {

    $('#resetPasswordForm').on('submit', resetPassword);
    $('#resetPasswordForm [name="new_password"], #resetPasswordForm [name="confirm_password"]').on('change', checkConfirmPassword);

    async function resetPassword(e) {
        e.preventDefault();

        if (!util.form.checkValidity(this)) {
            return;
        }

        let data = util.form.getData(this);

        let result = await util.put('/api/profile/reset', data);

        if (!result.success) {
            return util.showError(result.msg);
        }

        util.location.replace('/profile?is_success');
    }

    function checkConfirmPassword() {
        const newPassword = $('#resetPasswordForm [name="new_password"]').val();
        const confirmPassword = $('#resetPasswordForm [name="confirm_password"]').val();
        
        if (newPassword === confirmPassword) {
            $('#resetPasswordForm [name="confirm_password"]').get(0).setCustomValidity('');
        } else {
            $('#resetPasswordForm [name="confirm_password"]').get(0).setCustomValidity('error');
        }
    }
});