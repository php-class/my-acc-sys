$(function () {
    const $table = $("#resultTable").bootstrapTable({
        idField: "id",
        paginationVAlign: "top",
        locale: navigator.language,
        paginationLoop: false,
        pagination: true,
        sidePagination: "server",
        pageSize: 10,
        pageList: [5, 10, 25],
        queryParams,
        responseHandler,
        columns: [
            {},
            {
                field: "date",
            },
            {
                field: "payment_method_desc",
            },
            {
                field: "type_desc",
            },
            {
                field: "amount",
                formatter: amountFormatter,
            },
            {
                field: "remark",
            },
            {
                field: "operate",
                formatter: operateFormatter,
                events: {
                    'click .edit': openModalEditData,
                },
            },
        ],
    });

    // 查詢提交
    $("#queryForm").on("submit", function (e) {
        e.preventDefault();
        $table.bootstrapTable("refresh", { pageNumber: 1 });
    });

    // 重置查詢條件
    $("#reset").click(function () {
        util.form.clearData("#queryForm");
        $("#queryForm [type=date]").prop('min', null);
        $("#queryForm [type=date]").prop('max', null);
        $table.bootstrapTable("refresh", { pageNumber: 1 });
    });

    // 當日期設定時，確保不起日不會超過迄日設定
    $("[name=dateFm], [name=dateTo]").change(function () {
        if (this.name === "dateFm") {
            $("[name=dateTo]").get(0).min = this.value;
        } else {
            $("[name=dateFm]").get(0).max = this.value;
        }
    });

    $('.modal').on('hidden.bs.modal', function() {
        util.form.clearData(this);
        util.form.clearValidity($(this).find('form'));
    });

    $('#modalExport').on('hidden.bs.modal', function() {
        util.form.resetValueToOri(this);
    });

    // 從後端獲取費種類的設定
    util.get("/api/expenditure/type").then(result => {
        $("select[name=type]").html("<option></option>");

        if (!result.success) {
            return;
        }

        for (let row of result.data) {
            $("select[name=type]").append(`<option value='${row.code}'>${row.desc}</option>`);
        }
    })

    // 從後端獲取付費方式設定
    util.get("/api/expenditure/payment").then(result => {
        $("select[name=payment_method]").html("<option></option>");

        if (!result.success) {
            return;
        }

        for (let row of result.data) {
            $("select[name=payment_method]").append(`<option value='${row.code}'>${row.desc}</option>`);
        }
    });

    function queryParams(params) {
        let { limit, offset } = params;
        let data = util.form.getData("#queryForm", null, true);

        return { ...data, limit, offset };
    }

    function responseHandler(res) {
        return {
            rows: res.data,
            total: res.total,
        };
    }

    function amountFormatter(value, row, index) {
        return new Intl.NumberFormat("en-IN").format(value);
    }

    function operateFormatter(value, row, index) {
        return `
            <div>
                <button class="btn btn-success edit" data-id="${row.id}">
                    <i class="bi bi-pencil-square"></i> ${this.editTitle}
                </button>
            </div>
        `;
    }

    function onLoadSuccess() {
        $("#resultTable button.edit").click(openModalEditData);
    }

    // 打開編輯視窗
    async function openModalEditData(e, value, row, index) {
        $('#modalEdit').find('[name="id"]').val(row.id);
        
        let result = await util.get(`/api/expenditure/${row.id}`);

        if (!result.success) {
            return util.showError(result.msg);
        }

        util.form.initData($('#modalEdit form'), result.data);

        $('#modalEdit').modal('show');
    }


    $('#editForm').on('submit', async function(e) {
        e.preventDefault();
        
        if (!util.form.checkValidity(this)) {
            return;
        }

        const data = util.form.getData(this, true);

        if ($.isEmptyObject(data)) {
            return;
        }

        util.spinner.show($(this).find('[type=submit]'));
        $(this).find(':input').prop('disabled', true);

        data.id = $(this).find('[name=id]').val();

        let result = await util.post(`/api/expenditure`, data);

        util.spinner.close($(this).find('[type=submit]'));
        $(this).find(':input').prop('disabled', false);
        
        if (!result.success) {
            return util.showError(result.msg);
        }

        $('#modalEdit').modal('hide');
        util.showSuccess(result.msg);
        $table.bootstrapTable("refresh");
    });

    $('#addForm').on('submit', async function(e) {
        e.preventDefault();

        if (!util.form.checkValidity(this)) {
            return;
        }

        let data = util.form.getData(this, null, true);

        util.spinner.show($(this).find('[type=submit]'));
        $(this).find(':input').prop('disabled', true);

        let result = await util.post(`/api/expenditure`, data);

        util.spinner.close($(this).find('[type=submit]'));
        $(this).find(':input').prop('disabled', false);
        
        if (!result.success) {
            return util.showError(result.msg);
        }

        $('#modalAdd').modal('hide');
        util.showSuccess(result.msg);
        $table.bootstrapTable("refresh", { pageNumber: 1 });
    });

    $('#exportForm').on('submit', async function(e) {
        e.preventDefault();

        if (!util.form.checkValidity(this)) {
            return;
        }

        let data = util.form.getData(this, null, true);

        if ($.isEmptyObject(data)) {
            return;
        }

        util.spinner.show($(this).find('[type=submit]'));
        $(this).find(':input').prop('disabled', true);

        let result = await util.download(`/api/expenditure/export/${data.date}`);

        util.spinner.close($(this).find('[type=submit]'));
        $(this).find(':input').prop('disabled', false);

        if (!result.success) {
            return util.showError(result.msg);
        }
        
        $(this).closest('.modal').modal('hide');
    });

    $("#btnDelete").click(async function() {
        
        let rows = $table.bootstrapTable('getSelections').map(({ id }) => id);
        if (!rows.length) {
            return;
        }

        if ($(this).is('[data-delete-confirm-msg]')) {
            let isConfirm = await util.confirm($(this).attr('data-delete-confirm-msg'));
            if (!isConfirm) {
                return;
            }
        }

        let result = await util.delete('/api/expenditure', { rows });

        if (!result.success) {
            return util.showError(result.msg);
        }

        $table.bootstrapTable("refresh", { pageNumber: 1 });
        return util.showSuccess(result.msg);
    });
});
