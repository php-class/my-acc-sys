$(function() {
    const $table = $("#resultTable").bootstrapTable({
        idField: "id",
        paginationVAlign: "top",
        locale: navigator.language,
        paginationLoop: false,
        pagination: true,
        sidePagination: "server",
        pageSize: 10,
        pageList: [5, 10, 25],
        queryParams,
        responseHandler,
        columns: [
            {},
            {
                field: "code",
            },
            {
                field: "type",
            },
            {
                field: "desc",
            },
            {
                field: "is_active",
                formatter: function(value, row, index) {
                    if (value == 1) {
                        return `<span class="light-green">${this.yes}</span>`;
                    }

                    return `<span class="light-red">${this.no}</span>`;
                },
            },
            {
                field: "operate",
                formatter: operateFormatter,
                events: {
                    'click .edit': openModalEditData,
                }
            },
        ],
    });

    function queryParams(params) {
        let { limit, offset } = params;
        let data = util.form.getData("#queryForm", void 0, true);

        return { ...data, limit, offset };
    }

    function responseHandler(res) {
        return {
            rows: res.data,
            total: res.total,
        };
    }

    $("#btnDelete").click(async function() {
        
        let rows = $table.bootstrapTable('getSelections').map(({ id }) => id);
        if (!rows.length) {
            return;
        }

        if ($(this).is('[data-delete-confirm-msg]')) {
            let isConfirm = await util.confirm($(this).attr('data-delete-confirm-msg'));
            if (!isConfirm) {
                return;
            }
        }

        let result = await util.delete('/api/admin/config', { rows });

        if (!result.success) {
            return util.showError(result.msg);
        }

        $table.bootstrapTable("refresh", { pageNumber: 1 });
        return util.showSuccess(result.msg);
    });

    function operateFormatter(value, row, index) {
        return `
            <div>
                <button class="btn btn-success edit" data-id="${row.id}">
                    <i class="bi bi-pencil-square"></i> ${this.editTitle}
                </button>
            </div>
        `;
    }

    $('.modal').on('hidden.bs.modal', function() {
        util.form.clearData(this);
        util.form.clearValidity($(this).find('form'));
    });

    // 打開編輯視窗
    async function openModalEditData(e, value, row, index) {
        $('#modalEdit').find('[name="id"]').val(row.id);
        
        let result = await util.get(`/api/admin/config/${row.id}`);

        if (!result.success) {
            return util.showError(result.msg);
        }

        util.form.initData($('#modalEdit form'), result.data);

        $('#modalEdit').modal('show');
    }

    // 查詢提交
    $("#queryForm").on("submit", function (e) {
        e.preventDefault();
        $table.bootstrapTable("refresh", { pageNumber: 1 });
    });

    // 重置查詢條件
    $("#reset").click(function () {
        util.form.clearData("#queryForm");
        $("#queryForm [type=date]").prop('min', null);
        $("#queryForm [type=date]").prop('max', null);
        $table.bootstrapTable("refresh", { pageNumber: 1 });
    });

    $('#addForm, #editForm').on('submit', async function(e) {
        e.preventDefault();

        if (!util.form.checkValidity(this)) {
            return;
        }

        let data = util.form.getData(this, undefined, true);

        let result = await util.post(`/api/admin/config`, data);
        
        if (!result.success) {
            return util.showError(result.msg);
        }

        $(this).closest('.modal').modal('hide');
        util.showSuccess(result.msg);

        let option = {};
        if (!data.id) {
            option.pageNumber = 1;
        }
        $table.bootstrapTable("refresh", option);
    });

    util.get('/api/admin/config/type')
        .then(result => {
            if (!result.success) {
                return;
            }

            $('select[name=type]').html('<option></option>');
            for (const type of result.data) {
                $('select[name=type]').append(`<option>${type}</option>`);
            } 
        });
});