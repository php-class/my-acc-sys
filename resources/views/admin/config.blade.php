@extends('layout.default')

@section('style')
<!-- <link rel="stylesheet" href="{{ asset('/css/config.css') }}"> -->
@endsection


@section('script')
<script src="{{ asset('/js/config.js') }}"></script>
@endsection

@section('main')
<div class="m-3">
    <div class="row g-3">
        <form id="queryForm">
            <div class="row row-cols-md-auto g-2 align-items-center mb-2 justify-content-center">
                <div class="col-12">
                    <div class="input-group">
                        <span class="input-group-text">{{ __('code') }}</span>
                        <input type="text" class="form-control text-uppercase" name="code">
                    </div>
                </div>
                <div class="col-12">
                    <div class="input-group">
                        <span class="input-group-text">{{ __('type') }}</span>
                        <select class="form-select" name="type">
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="input-group">
                        <span class="input-group-text">{{ __('status') }}</span>
                        <select class="form-select" name="is_active">
                            <option value=""></option>
                            <option value="1">{{ __('active') }}</option>
                            <option value="0">{{ __('close') }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-outline-secondary">{{ __('query') }}</button>
                    <button id="reset" class="btn btn-secondary">{{ __('reset') }}</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div id="toolbar">
            <button class="btn btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#modalAdd">
                <i class="bi bi-plus"></i>
                {{ __('add') }}
            </button>
            <button class="btn btn-danger" id="btnDelete" data-delete-confirm-msg="{{ __('delete.confirm') }}">
                <i class="bi bi-trash"></i>
                {{ __('delete') }}
            </button>
        </div>
        <table id="resultTable" 
            data-url="{{ url('/api/admin/config') }}"
            data-toolbar="#toolbar"
        >
            <thead>
                <tr>
                    <th data-checkbox="true"></th>
                    <th data-field="code" data-halign="center">{{ __('code') }}</th>
                    <th data-field="type" data-halign="center">{{ __('type') }}</th>
                    <th data-field="desc" data-halign="center">{{ __('desc') }}</th>
                    <th data-field="is_active" data-halign="center" data-align="center" data-yes="{{ __('active') }}" data-no="{{ __('close') }}">Lang.status</th>
                    <th data-halign="center" data-edit-title="{{ __('edit') }}">
                        {{ __('operate') }}
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div id="modalEdit" class="modal fade" tabindex="-1">
    <form id="editForm" novalidate class="needs-validation">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('edit') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3 row">
                        <label for="date" class="col-sm-2 col-form-label">{{ __('code') }}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control text-uppercase" name="code" required data-require-msg="{{ __('require.code') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('type') }}</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control text-uppercase" name="type" required data-require-msg="{{ __('require.type') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('desc') }}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="desc" required data-require-msg="{{ __('require.desc') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="amount" class="col-sm-2 col-form-label">{{ __('status') }}</label>
                        <div class="col-sm-10">
                            <select name="is_active" class="form-select">
                                <option value="1">{{ __('active') }}</option>
                                <option value="0">{{ __('close') }}</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="id" />
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-secondary">{{ __('save') }}</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="modalAdd" class="modal fade" tabindex="-1">
    <form id="addForm" novalidate class="needs-validation">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('add') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                <div class="mb-3 row">
                        <label for="date" class="col-sm-2 col-form-label">{{ __('code') }}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control text-uppercase" name="code" required data-require-msg="{{ __('require.code') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('type') }}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control text-uppercase" name="type" required data-require-msg="{{ __('require.type') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('desc') }}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="desc" required data-require-msg="{{ __('require.desc') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="amount" class="col-sm-2 col-form-label">{{ __('status') }}</label>
                        <div class="col-sm-10">
                            <select name="is_active" class="form-select">
                                <option value="1">{{ __('active') }}</option>
                                <option value="0">{{ __('close') }}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-secondary">{{ __('create') }}</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection