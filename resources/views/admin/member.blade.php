@extends('layout.default')

@section('style')
<link rel="stylesheet" href="{{ asset('/css/member.css') }}">
@endsection


@section('script')
<script src="{{ asset('/js/member.js') }}"></script>
@endsection


@section('main')
<div class="m-3">
    <div class="row g-3">
        <form id="queryForm">
            <div class="row row-cols-md-auto g-2 align-items-center mb-2 justify-content-center">
                <div class="col-12">
                    <div class="input-group">
                        <span class="input-group-text">{{ __('role') }}</span>
                        <select name="role" class="form-select">
                            <option></option>
                            <option value="ADMIN">{{ __('role.admin') }}</option>
                            <option value="GENERAL">{{ __('role.general') }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="input-group">
                        <span class="input-group-text">{{ __('status') }}</span>
                        <select name="is_active" class="form-select">
                            <option></option>
                            <option value="1">{{ __('active') }}</option>
                            <option value="0">{{ __('close') }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="input-group">
                        <span class="input-group-text">{{ __('last.login') }}</span>
                        <input type="date" class="form-control" name="date_fm">
                        <input type="date" class="form-control" name="date_to">
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-outline-secondary">{{ __('query') }}</button>
                    <button id="reset" class="btn btn-secondary">{{ __('reset') }}</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <table id="resultTable" 
            data-url="{{ url('/api/admin/member') }}"
            data-yes="{{ __('yes') }}"
            data-no="{{ __('no') }}"
        >
            <thead>
                <tr>
                    <th data-field="role" data-halign="center" data-align="center" data-role-admin="{{ __('role.admin') }}" data-role-general="{{ __('role.general') }}">{{ __('role') }}</th>
                    <th data-field="account" data-halign="center" data-align="center">{{ __('account') }}</th>
                    <th data-field="is_active" data-halign="center" data-align="center" data-yes="{{ __('active') }}" data-no="{{ __('close') }}">{{ __('status') }}</th>
                    <th data-field="last_login" data-halign="center" data-align="center">{{ __('last.login') }}</th>
                    <th data-halign="center" data-edit-title="{{ __('edit') }}" data-random-password-title="{{ __('random.password') }}">{{ __('operate') }}</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalEdit" class="modal fade" tabindex="-1">
    <form id="editForm" novalidate class="needs-validation">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('edit') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('account') }}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control-plaintext" name="account" readonly>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('role') }}</label>
                        <div class="col-sm-10">
                            <select name="role" class="form-select" required data-require-msg="{{ __('require.select.role') }}">
                                <option value="ADMIN">{{ __('role.admin') }}</option>
                                <option value="GENERAL">{{ __('role.general') }}</option>
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('status') }}</label>
                        <div class="col-sm-10">
                            <select name="is_active" class="form-select" required data-require-msg="{{ __('require.select.status') }}">
                                <option value="1">{{ __('active') }}</option>
                                <option value="0">{{ __('close') }}</option>
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <input type="hidden" name="id" />
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-secondary">{{ __('save') }}</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="modalNewPassword" class="modal fade" data-bs-backdrop="static" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('new.password') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="bg-opacity-10 bg-black p-2 d-block w-100 rounded-3 ">
                    <code id="newPassword"></code>
                    <button id="btnCopy" class='btn btn-link float-end' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="{{ __('copy') }}" data-bs-trigger="hover"><i class="bi bi-clipboard2"></i></button>
                </div>
            </div>
            <div class="modal-footer">
                <button data-bs-dismiss="modal" class="btn btn-outline-secondary">{{ __('close') }}</button>
            </div>
        </div>
    </div>
</div>
@endsection