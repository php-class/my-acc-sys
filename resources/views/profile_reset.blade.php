@extends('layout.default')

@section('script')
<script src="{{ asset('/js/reset_password.js')}}"></script>
@endsection

@section('main')
    <div class="container">
        <div class="row my-3">
            <div class="col h1" style="line-height: 60px;">
                {{ __('reset.password') }}
            </div>
            <hr class="mt-2">
        </div>
        <div class="row p-2">
            <div class="card m-auto col-sm-8 col-md-6 col-lg-4">
                <div class="card-body">
                    <form id="resetPasswordForm" class="needs-validation g-3 row" novalidate>
                        <div class="col-12">
                            <label class="form-label">{{ __('password.ori') }}</label>
                            <input type="password" class="form-control" name="password" required data-require-msg="{{ __('require.password') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="col-12">
                            <label class="form-label">{{ __('password.new') }}</label>
                            <input type="password" class="form-control" name="new_password" required
                                pattern="^((?=.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*|(?=.{8,}$)(?=.*\d)(?=.*[a-zA-Z]).*)"
                                data-require-msg="{{ __('require.new.password') }}" 
                                data-pattern-msg="{{ __('pattern.error.new.password') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class=" text-black-50">
                            <strong>{{ __('password.hint1') }}</strong>
                            {{ __('password.hint2') }}
                        </div>
                        <div class="col-12">
                            <label class="form-label">{{ __('password.new.confirm') }}</label>
                            <input type="password" class="form-control" name="confirm_password" required
                                pattern="^((?=.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*|(?=.{8,}$)(?=.*\d)(?=.*[a-zA-Z]).*)"
                                data-require-msg="{{ __('require.new.confirm.password') }}" 
                                data-pattern-msg="{{ __('pattern.error.new.confirm.password') }}"
                                data-custom-msg="{{ __('password.not.same') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="text-end col-12">
                            <button type="submit" class="btn btn-outline-secondary">{{ __('modify') }}</button>
                            <button type="button" class="btn btn-secondary" onclick="window.history.back();">{{ __('back') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection