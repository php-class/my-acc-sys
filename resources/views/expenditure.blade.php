@extends('layout.default')

@section('style')
<link rel="stylesheet" href="{{ asset('/css/expenditure.css') }}">
@endsection

@section('script')
<script src="{{ asset('/js/expenditure.js') }}"></script>
@endsection

@section('main')
<div class="m-3">
    <div class="row g-3">
        <form id="queryForm">
            <div class="row row-cols-md-auto g-2 align-items-center mb-2 justify-content-center">
                <div class="col-12">
                    <div class="input-group">
                        <span class="input-group-text">{{ __('date') }}</span>
                        <input type="date" class="form-control" name="dateFm">
                        <input type="date" class="form-control" name="dateTo">
                    </div>
                </div>
                <div class="col-12">
                    <div class="input-group">
                        <span class="input-group-text">{{ __('payment.method') }}</span>
                        <select class="form-select" name="payment_method">
                            <option></option>
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="input-group">
                        <span class="input-group-text">{{ __('expenditure.type') }}</span>
                        <select class="form-select" name="type">
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-outline-secondary">{{ __('query') }}</button>
                    <button id="reset" class="btn btn-secondary">{{ __('reset') }}</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div id="toolbar">
            <button class="btn btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#modalAdd">
                <i class="bi bi-plus"></i>
                {{ __('add') }}
            </button>
            <button class="btn btn-danger" id="btnDelete" data-delete-confirm-msg="{{ __('delete.confirm') }}">
                <i class="bi bi-trash"></i>
                {{ __('delete') }}
            </button>
            <button class="btn btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#modalExport">
                <i class="bi bi-box-arrow-up"></i>
                {{ __('export') }}
            </button>
        </div>
        <table id="resultTable" 
            data-url="{{ url('/api/expenditure') }}"
            data-toolbar="#toolbar"
        >
            <thead>
                <tr>
                    <th data-checkbox="true"></th>
                    <th data-field="date" data-halign="center" data-width="100" data-align="center">{{ __('date') }}</th>
                    <th data-field="payment_method_desc" data-width="100" data-halign="center" data-align="center">{{ __('payment.method') }}</th>
                    <th data-field="type_desc" data-width="100" data-halign="center" data-align="center">{{ __('expenditure.type') }}</th>
                    <th data-field="amount" data-halign="center" data-width="150" data-align="right">{{ __('amount') }}</th>
                    <th data-field="remark" data-halign="center" data-align="left">{{ __('remark') }}</th>
                    <th data-halign="center" data-width="80" data-align="center" data-edit-title="{{ __('edit') }}">{{ __('operate') }}</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalEdit" class="modal fade" tabindex="-1">
    <form id="editForm" novalidate class="needs-validation">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('edit') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3 row">
                        <label for="date" class="col-sm-2 col-form-label">{{ __('date') }}</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="date" required data-require-msg="{{ __('require.date') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('payment.method') }}</label>
                        <div class="col-sm-10">
                            <select name="payment_method" class="form-select" required data-require-msg="{{ __('require.select.payment.method') }}">
                                <option></option>
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('expenditure.type') }}</label>
                        <div class="col-sm-10">
                            <select name="type" class="form-select" required data-require-msg="{{ __('require.select.expenditure.type') }}">
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="amount" class="col-sm-2 col-form-label">{{ __('amount') }}</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="amount">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('remark') }}</label>
                        <div class="col-sm-10">
                            <textarea name="remark" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="id" />
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-secondary">{{ __('save') }}</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="modalAdd" class="modal fade" tabindex="-1">
    <form id="addForm" novalidate class="needs-validation">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('add') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3 row">
                        <label for="date" class="col-sm-2 col-form-label">{{ __('date') }}</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="date" required data-require-msg="{{ __('require.date') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('payment.method') }}</label>
                        <div class="col-sm-10">
                            <select name="payment_method" class="form-select" required data-require-msg="{{ __('require.select,payment.method') }}">
                                <option></option>
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('expenditure.type') }}</label>
                        <div class="col-sm-10">
                            <select name="type" class="form-select" required data-require-msg="{{ __('require.select.expenditure.type') }}">
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="amount" class="col-sm-2 col-form-label">{{ __('amount') }}</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="amount">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">{{ __('remark') }}</label>
                        <div class="col-sm-10">
                            <textarea name="remark" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-secondary">{{ __('create') }}</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="modalExport" class="modal fade" tabindex="-1">
    <form id="exportForm" novalidate class="needs-validation">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('export') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3 row">
                        <label for="date" class="col-sm-2 col-form-label">{{ __('date') }}</label>
                        <div class="col-sm-10">
                            <input type="month" data-ori-value="<?= date('Y-m') ?>" value="<?= date('Y-m') ?>" class="form-control" name="date" required data-require-msg="{{ __('require.date') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                    <button type="submit" class="btn btn-outline-secondary">{{ __('export') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection