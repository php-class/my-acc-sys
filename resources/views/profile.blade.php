@extends('layout.default')

@section('style')
<link rel="stylesheet" href="{{ asset('/css/profile.css') }}">
@endsection

@section('script')
<script src="{{ asset('/js/profile.js') }}"></script>
@endsection

@section('main')
    <div class="container">
        <div class="row my-3 avatar" >
            <div class="col-12">
                <i class="bi bi-person-circle"></i> 
                <img id="avatar" src="" alt="" width="120px" height="120px" class="rounded-circle" style="display: none;">
            </div>
            <div class="col-12 mt-2">
                <div style="width: 120px;" class="text-center">
                    <button class="btn btn-sm btn-outline-secondary" id="btnPreview">{{ __('avatar.upload') }}</button>
                </div>
                <input type="file" name="file" id="file" class="d-none" accept="image/*">
            </div>
            <hr class="mt-2">
        </div>
        <div class="row">
            <?php if (isset($_GET['is_success'])): ?>
                <div class="msg mb-2 overflow-hidden w-100">
                    <x-alert type="success" message="{{ __('success.reset.password') }}"></x-alert>
                </div>
            <?php endif ?>
            <div class="card m-auto col-sm-6 col-md-6 col-lg-4">
                <div class="card-body">
                    <form id="profileForm">
                        <div class="mb-3">
                            <label class="form-label">{{ __('account') }}</label>
                            <input type="text" readonly class="form-control-plaintext" name="account" >
                        </div>
                        <div class="mb-3">
                            <label class="form-label">{{ __('password') }}</label>
                            <div class="input-group">
                                <input type="text" readonly class="form-control" value="********">
                                <a class="btn btn-outline-secondary" href="{{ url('/profile/reset') }}">{{ __('reset.password') }}</a>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-lg-6 col-12">
                                <label class="form-label">{{ __('last.name') }}</label>
                                <input type="text" class="form-control" name="first_name" >
                            </div>
                            <div class="col-lg-6 col-12">
                                <label class="form-label">{{ __('first.name') }}</label>
                                <input type="text" class="form-control" name="last_name" >
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">{{ __('birthday') }}</label>
                            <input type="date" class="form-control" name="birthday" >
                        </div>
                        <div class="mb-3 text-end">
                            <button type="submit" class="btn btn-outline-secondary">{{ __('modify') }}</button>
                        </div>
                        <input type="hidden" name="avatar"/>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div id="modalUpload" class="modal fade" tabindex="-1">
        <form id="uploadForm" novalidate class="needs-validation">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('avatar.upload') }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3 row">
                            <div class="col-sm-12">
                                <input accept="image/*" type="file" class="form-control" name="file" required data-require-msg="{{ __('require.select.file') }}">
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-secondary">{{ __('upload') }}</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div id="modalPreview" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('avatar.upload') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="position-relative m-auto" style="width: 350px; height: 350px">
                        <img src="" alt="" id="preview" height="350px" width="350px" style="object-fit: cover; left: 0; top: 0;" class="rounded-circle position-absolute">
                        <img src="" alt="" id="previewBg" height="350px" width="350px" style="object-fit: cover; left: 0; top: 0;" class="opacity-25 position-absolute">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-secondary" id="btnUpload">{{ __('upload') }}</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
    @endsection