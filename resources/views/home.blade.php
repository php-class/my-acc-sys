@extends('layout.default')

@section('style')
<link rel="stylesheet" href="{{ asset('/css/home.css') }}">
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/chart.js@4.1.1/dist/chart.umd.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.2.0"></script>
<script src="{{ asset('/js/home.js') }}"></script>
@endsection

@section('main')
<div class="container p-4">
    <div class="row mb-3">
        <div class="col">
            <div class="statistics" style="height: 400px;">
                <canvas data-title="{{ __('statistics.title') }}" data-x-label="{{ __('date') }}" data-y-label="{{ __('amount') }}" data-label="{{ __('expenditure') }}" id="statistics" class="w-100"></canvas>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="statistics">
                <canvas data-title="{{ __('statistics.type.title') }}" data-label="{{ __('count') }}" id="statisticsByType"></canvas>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="statistics">
                <canvas data-title="{{ __('statistics.payment.title') }}" data-label="{{ __('count') }}" id="statisticsByPaymentMethod"></canvas>
            </div>
        </div>
    </div>
</div>
@endsection