<aside class="collapse sidebar text-bg-light position-fixed col-lg-2 d-lg-block bg-light" tabindex="-1" id="sidebar">
    <nav class="pt-3 d-flex flex-column">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link {{ url()->current() === url('/') ? 'active' : null}}" href="{{ url('/') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-speedometer2 align-text-bottom" viewBox="0 0 16 16">
                        <path d="M8 4a.5.5 0 0 1 .5.5V6a.5.5 0 0 1-1 0V4.5A.5.5 0 0 1 8 4zM3.732 5.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 10a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 10zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 9.31a.91.91 0 1 0 1.302 1.258l3.434-4.297a.389.389 0 0 0-.029-.518z"/>
                        <path fill-rule="evenodd" d="M0 10a8 8 0 1 1 15.547 2.661c-.442 1.253-1.845 1.602-2.932 1.25C11.309 13.488 9.475 13 8 13c-1.474 0-3.31.488-4.615.911-1.087.352-2.49.003-2.932-1.25A7.988 7.988 0 0 1 0 10zm8-7a7 7 0 0 0-6.603 9.329c.203.575.923.876 1.68.63C4.397 12.533 6.358 12 8 12s3.604.532 4.923.96c.757.245 1.477-.056 1.68-.631A7 7 0 0 0 8 3z"/>
                    </svg>
                    {{ __('dashboard') }}
                </a>
                <a class="nav-link {{ url()->current() === url('/expenditure') ? 'active' : null}}" href="{{ url('/expenditure') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-cash-coin align-text-bottom" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M11 15a4 4 0 1 0 0-8 4 4 0 0 0 0 8zm5-4a5 5 0 1 1-10 0 5 5 0 0 1 10 0z"/>
                        <path d="M9.438 11.944c.047.596.518 1.06 1.363 1.116v.44h.375v-.443c.875-.061 1.386-.529 1.386-1.207 0-.618-.39-.936-1.09-1.1l-.296-.07v-1.2c.376.043.614.248.671.532h.658c-.047-.575-.54-1.024-1.329-1.073V8.5h-.375v.45c-.747.073-1.255.522-1.255 1.158 0 .562.378.92 1.007 1.066l.248.061v1.272c-.384-.058-.639-.27-.696-.563h-.668zm1.36-1.354c-.369-.085-.569-.26-.569-.522 0-.294.216-.514.572-.578v1.1h-.003zm.432.746c.449.104.655.272.655.569 0 .339-.257.571-.709.614v-1.195l.054.012z"/>
                        <path d="M1 0a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4.083c.058-.344.145-.678.258-1H3a2 2 0 0 0-2-2V3a2 2 0 0 0 2-2h10a2 2 0 0 0 2 2v3.528c.38.34.717.728 1 1.154V1a1 1 0 0 0-1-1H1z"/>
                        <path d="M9.998 5.083 10 5a2 2 0 1 0-3.132 1.65 5.982 5.982 0 0 1 3.13-1.567z"/>
                    </svg>
                    {{ __('expenditure') }}
                </a>
            </li>
        </ul>
        @if(auth()->user()->isAdmin())
            <hr/>
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link {{ url()->current() === url('/admin/member') ? 'active' : null}}" href="{{ url('/admin/member') }}">
                        <i class="bi bi-people-fill"></i>
                        {{ __('member.management') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ url()->current() === url('/admin/config') ? 'active' : null}}" href="{{ url('/admin/config') }}">
                        <i class="bi bi-sliders2-vertical"></i>
                        {{ __('config') }}
                    </a>
                </li>
            </ul>
        @endif
        <hr class="mt-auto"/>
        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link {{ url()->current() === url('/profile') ? 'active' : null}}" href="{{ url('/profile') }}">
                    <i class="bi bi-person-circle"></i>
                    {{ __('profile.center') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/api/logout') }}">
                    <i class="bi bi-door-open"></i>
                    {{ __('logout') }}
                </a>
            </li>
        </ul>
    </nav>
</aside>