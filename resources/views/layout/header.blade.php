<header class="fixed-top p-0 bg-dark w-100">
    <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container-fluid">
            @guest
                <a class="navbar-brand logo" href="#">
                    <img src="{{ asset('/images/catH.png') }}" alt="Logo" class="d-inline-block align-text-top">
                </a>
            @endguest
            <button class="navbar-toggler border-0 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebar" aria-controls="sidebar">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </nav>
</header>