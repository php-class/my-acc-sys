@extends('layout.default')

@section('style')
    <link rel="stylesheet" href="{{asset('/')}}{{mix('/css/login.css')}}">
@endsection
@section('script')
    <script src="{{asset('/')}}{{mix('/js/login.js')}}"></script>
@endsection

@section('main')
<div>
    <section class="user">
        <div class="user_options-container">
            <div class="user_options-text">
                <div class="user_options-unregistered">
                    <h2 class="user_unregistered-title">{{ __('register.hint') }}</h2>
                    <button class="user_unregistered-signup" id="signup-button">{{ __('register') }}</button>
                </div>

                <div class="user_options-registered">
                    <h2 class="user_registered-title">{{ __('login.hint') }}</h2>
                    <button class="user_registered-login" id="login-button">{{ __('login' )}}</button>
                </div>
            </div>

            <div class="user_options-forms bounceRight" id="user_options-forms">
                <div class="user_forms-login">
                    <h2 class="forms_title">{{ __('login') }}</h2>
                    <form id="loginForm" class="forms_form needs-validation" novalidate>
                        @csrf
                        <fieldset class="forms_fieldset">
                            <div class="forms_field">
                                <input type="text" placeholder="{{ __('account') }}" name="account" class="forms_field-input" required autofocus
                                    data-require-msg="{{ __('require.account') }}"
                                    />
                                <div class="invalid-feedback"></div>
                            </div>
                            <div class="forms_field">
                                <input type="password" placeholder="{{ __('password') }}" name="password" class="forms_field-input" required
                                    data-require-msg="{{ __('require.password') }}" 
                                    />
                                <div class="invalid-feedback"></div>
                            </div>
                        </fieldset>
                        <div class="forms_buttons justify-content-end">
                            <!-- <button type="button" class="forms_buttons-forgot">Forgot password?</button> -->
                            <button id="btnLogin" type="submit" class="forms_buttons-action">{{ __('login') }}</button>
                        </div>
                    </form>
                </div>
                <div class="user_forms-signup">
                    <h2 class="forms_title">{{ __('register') }}</h2>
                    <form id="registerForm" class="forms_form needs-validation" novalidate>
                        @csrf
                        <fieldset class="forms_fieldset">
                            <div class="forms_field">
                                <input type="text" name="account" placeholder="{{ __('account') }}" class="forms_field-input" required data-min-length="8"
                                    data-require-msg="{{ __('require.account') }}"
                                    data-too-short-msg="{{ __('min.length.account') }}"/>
                                <div class="invalid-feedback"></div>
                            </div>
                            <div class="forms_field">
                                <input type="password" name="password" placeholder="{{ __('password') }}" class="forms_field-input" required 
                                    pattern="^((?=.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*|(?=.{8,}$)(?=.*\d)(?=.*[a-zA-Z]).*)"
                                    data-require-msg="{{ __('require.password') }}" 
                                    data-pattern-msg="{{ __('pattern.error.password') }}"/>
                                <div class="invalid-feedback"></div>
                            </div>
                            <div class="forms_field text-black-50">
                                <strong>{{ __('password.hint1') }}</strong>
                                {{ __('password.hint2') }}
                            </div>
                            <div class="forms_field">
                                <input type="password" name="password_confirmation" placeholder="{{ __('confirm.password') }}" class="forms_field-input" required 
                                    pattern="^((?=.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*|(?=.{8,}$)(?=.*\d)(?=.*[a-zA-Z]).*)"
                                    data-require-msg="{{ __('require.confirm.password') }}" 
                                    data-pattern-msg="{{ __('pattern.error.confirm.password') }}"
                                    data-custom-msg="{{ __('password.not.same') }}"/>
                                <div class="invalid-feedback"></div>
                            </div>
                        </fieldset>
                        <div class="forms_buttons">
                            <button type="submit" class="forms_buttons-action">{{ __('register') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection