# 個人記帳管理系統
 
 - 系統要求 `PHP` 7.4 含以上

### 程式介紹

 此系統我們引用了`Laravel`框架，在此我們針對幾個我們有使用到的目錄、檔案及配置進行介紹及講解，如果想瞭解更多完整的功能，可以前往官方網站進行查閱。

 - `public/index.php` 程式進入點。

    此為框架的程式進入點，跟我們以往所寫的進入點不同，以往的都是在根目錄底下的`index.php`，但框架的卻不是。而由於我們220上面無法直接指定程式的進入點目錄(public)，所以我透過框架提供的`server.php`來執行主程式，新增一個`index.php`來指向`server.php`。
    ```
    <?php
        header("Location: server.php");
    ```
 - `.env` 環境配置檔案，如需完整的配置設定可以查看當前目錄底下的`.env.example`檔案。如下
    - 當前系統的基本網址，例如 : `http://localhost/public/index.php`
    - 當前系統檔案資源的網址，讓我們放一些js或css的程式片段，例如 : `http://localhost/public`
    - 資料庫連接網址
    - 資料庫名稱
    - 資料庫使用者名稱
    - 資料庫使用者密碼
    - 資料庫種類，讓系統知道連接的資料庫是什麼資料庫。例如：`mysql`
    - 資料庫端口
    - 系統的模式，如下
        - `production` 生產模式。
        - `local` 開發/本機模式。在此模式下，可以使用 `/db` 路由，自動創建資料庫表格
 - `config` 設定檔案，在此目錄下放置了系統所需的系統檔案，但其實大部分的時候我們只需要透過`.env`配置就可以滿足整個系統的環境設定了。以下針對我有使用到以及修改到的設置進行講解。
    - `config/app.php` 在檔案中我們需修改的配置如下：
        - `locale` 預設的區域，根據配置使用預設的語系檔案提供服務。
        - `fallback_locale` 如果說我們所設定的語系是沒有的，則會使用此設定的語系檔案。
 - `app/Constant` 常量目錄，此為我自行新增的一個目錄，在這裡放了一些常量的配置程式，而常量的作用能夠幫助我們定義以及規範，某些特定的欄位之中只能存放特定的值，以及可以撰寫一些註釋，讓我們暸解其功能及目的性。例如：`UserRole`就是針對用戶的權限進行的規範及定義。
 - `app/Exceptions` 錯處處理，我們可以在此目錄底下的`Handler.php`針對我們的需求進行錯誤的處理。
 - `app/Http` 這裡放置這裡放置了我們程式所需用到的控制器(Controller)以及中間件(Middleware)。
    - `app/Http/Controllers` 控制器。主要負責具體的業務邏輯處理，例如資料的新增、刪除、查詢或者其他的功能，都在對應的Controller裡面進行操作。
    - `app/Http/Middleware` 中間件。在此可以新增我的所需要用的中間件，而中間件會在每次我的對系統發起請求時，根據各個請求所配置的中間件做一些我們所需要的處理。例如身份認證，我們就可以在中間件中進行統一的驗證及管理。而此框架本身就提供了身份認證的中間件`Authenticate.php`，以及我自行新增的`ApiAuthenticate.php`來針對API的請求處理來返回未認證的訊息。
    - `app/Http/Kernel.php` 上述所說的中間件，需要在此配置才能使用。
 - `app/Models` 資料庫的實際操作，將資料庫對應的表個信行一對一的封裝到Model裏面，每個Model裡面都有基礎的新增、修改、刪除及查詢功能，也可以自行定義新的功能或者自行撰寫特殊且複雜的SQL語句，讓我們更著重在數據的處理上。
 - `app/View` 提供了Html模塊化的功能，讓我們在我們的前端之中，可以像是寫Html的方式已用我的自己所建立的組件。例如我自行創建的`app/View/Components/Alert.php`透過配置後，我們在Html之中就可以直接透過`<x-alert></x-alert>`來引用我們自己寫的元件。
 - `database` 數據庫的創建及基礎資料的創建。
    - `database/migrations` 創建及修改表格，我們可以在此進行表的創建、欄位的定義以及相關的修改，來透過請求`/db`創建表格，或者蘭為新增或異動時，都能及時的修改及調整。
    - `database/seeders` 基礎資料的創建，可以在此建立一些基本的資料，一樣透過請求`/db`時，在創建表格之後一同的新增的資料庫中。
 - `resources` 資源檔，裡面放置了我們前端所需的頁面、js、cs以及區域的語系檔案。
 - `routes` 路由，根據請求的方式及條件，配置我們的回傳結果，例如我們在`routes/api.php`配置實際的業務功能，以及返回json的執行結果，而在`routes/web.php`根據網址返回對應頁面的結果。

 > 補充：routes以及app/Views的元件，都是透過app/Providers裡面的程式進行調用，讓我們在系統中能正常使用，而這些程式是框架一開始就內建的。
### 表結構介紹
 - `sys_users` 使用者
    | 欄位名稱      | 欄位類型  | 主鍵 | 預設值  | 自動遞增 | 空值 | 欄位敘述                                  |
    | ------------- | --------- | ---- | ------- | -------- | ---- | ----------------------------------------- |
    | id            | bigint    | 是   | 無      | 是       | 否   | 主鍵                                      |
    | account       | varchar   | 否   | 無      | 否       | 否   | 帳號                                      |
    | password      | varchar   | 否   | 無      | 否       | 否   | 密碼                                      |
    | is_active     | tinyint   | 否   | 1       | 否       | 否   | 裝態 1: 生效; 0: 關閉                     |
    | last_login_at | timestamp | 否   | NULL    | 否       | 是   | 最後登入時間                              |
    | last_name     | varchar   | 否   | NULL    | 否       | 是   | 姓氏                                      |
    | first_name    | varchar   | 否   | NULL    | 否       | 是   | 名字                                      |
    | birthday      | date      | 否   | NULL    | 否       | 是   | 生日                                      |
    | role          | varchar   | 否   | GENERAL | 否       | 否   | 角色權限 ADMIN: 管理員; GENERAL: 一般用戶 |
    | avatar        | varchar   | 否   | 無      | 否       | 是   | 頭像                                      |
    | created_at    | timestamp | 否   | 否      | 否       | 是     | 創建時間                                  |

 - `sys_configs` 配置
    | 欄位名稱   | 欄位類型  | 主鍵 | 預設值 | 自動遞增 | 空值 | 欄位敘述              |
    | ---------- | --------- | ---- | ------ | -------- | ---- | --------------------- |
    | id         | bigint    | 是   | 無     | 是       | 否   | 主鍵                  |
    | type       | varchar   | 否   | 無     | 否       | 否   | 類型                  |
    | code       | varchar   | 否   | 無     | 否       | 否   | 編碼                  |
    | desc       | varchar   | 否   | NULL   | 否       | 是   | 敘述                  |
    | is_active  | tinyint   | 否   | 1      | 否       | 否   | 狀態 1: 生效; 0: 關閉 |
    | created_at | timestamp | 否   | 無     | 否       | 是   | 創建時間              |
    | updated_at | timestamp | 否   | 無     | 否       | 是   | 修改時間              |
    | created_by | bigint    | 否   | 無     | 否       | 是   | 創建人              |
    | updated_by | bigint    | 否   | 無     | 否       | 是   | 修改人                      |
 
 - `sys_expenditures` 支出
    | 欄位名稱       | 欄位類型  | 主鍵 | 預設值 | 自動遞增 | 空值 | 欄位敘述 |
    | -------------- | --------- | ---- | ------ | -------- | ---- | -------- |
    | id             | bigint    | 是   | 無     | 是       | 否   | 主鍵     |
    | payment_method | varchar   | 否   | NULL   | 否       | 是   | 付款方式 |
    | amount         | decimal   | 否   | 0      | 否       | 否   | 金額     |
    | type           | varchar   | 否   | NULL   | 否       | 是   | 種類     |
    | remark         | varchar   | 否   | NULL   | 否       | 是   | 備註     |
    | date           | date      | 否   | 無     | 否       | 否   | 日期     |
    | created_by     | bigint    | 否   | NULL   | 否       | 是   | 創建人   |
    | updated_by     | bigint    | 否   | NULL   | 否       | 是   | 修改人   |
    | created_at     | timestamp | 否   | NULL   | 否       | 是   | 創建日期 |
    | updated_at     | timestamp | 否   | NULL   | 否       | 是   | 修改日期 |

 - `sys_files` 檔案
    | 欄位名稱   | 欄位類型  | 主鍵 | 預設值 | 自動遞增 | 空值 | 欄位敘述             |
    | ---------- | --------- | ---- | ------ | -------- | ---- | -------------------- |
    | id         | bigint    | 是   | 無     | 是       | 否   | 主鍵                 |
    | path       | varchar   | 否   | 無     | 否       | 否   | 存放路徑             |
    | uuid       | char      | 否   | 無     | 否       | 否   | 用於對外獲取的唯一id |
    | type       | varchar   | 否   | NULL   | 否       | 是   | 檔案類型             |
    | size       | bigint    | 否   | NULL   | 否       | 是   | 檔案大小             |
    | name       | varchar   | 否   | 無     | 否       | 否   | 檔案名稱             |
    | created_at | timestamp | 否   | NULL   | 否       | 是   | 創建日期             |
    | updated_at | timestamp | 否   | NULL   | 否       | 是   | 修改日期             |

### 引用資源
 - [Laravel](https://laravel.com/) 後端Php框架。
 - [PhpSpreadsheet](https://phpspreadsheet.readthedocs.io/en/latest/) 後段Php 製作Excel工具包。
 - [jQuery](https://jquery.com/) 前端js框架。
 - [Bootstrap](https://getbootstrap.com/) 前端Css工具包。
 - [Bootstrap Table](https://bootstrap-table.com/) 基於Bootstrap的前端table工具包。
 - [moment.js](https://momentjs.com/) 前端時間相關操作的工具包。
 - [popper.js](https://popper.js.org/) 用於前端Bootstrap中的tooltip所須引入的第三方工具。
 - [bootbox.js](http://bootboxjs.com/) 提供可客製化對話欄操作，來取代傳統的對話框，例如`alert`、`confirm`等等。
 - [FileSaver.js](https://github.com/eligrey/FileSaver.js/) 前端下載功能的實作工具包。