const mix = require('laravel-mix');
const glob = require('glob');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
const jsFiles = glob.sync('resources/js/*.js');
for (const file of jsFiles) {
    if (file === 'resources/js/bootstrap.js') {
        continue;
    }
    mix.js(file, 'public/js');
}

const cssFiles = glob.sync('resources/css/*.css');
for (const file of cssFiles) {
    mix.postCss(file, 'public/css');
}