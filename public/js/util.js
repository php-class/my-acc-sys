/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!******************************!*\
  !*** ./resources/js/util.js ***!
  \******************************/
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i["return"] && (_r = _i["return"](), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
var baseUrl = document.currentScript.dataset.baseUrl;
$(function () {
  /**
   * 獲取對象，並轉換成jQuery實例返回。
   * @param {*} target 
   * @returns 
   */
  function _getTarget(target) {
    // 如果傳入的對象為字符串或者原生的html元素，則加上jQuery;
    if (typeof target === 'string' || target instanceof HTMLElement) {
      return $(target);
    }
    // 如果目標為jQuery實例，則直些返回。
    if (target instanceof jQuery) {
      return target;
    }
    return null;
  }

  /**
   * 右上角的提示框
   * @param {String} type 
   * @param {String} msg
   * @param {Number} wait
   */
  function _showMsg(type) {
    var msg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    var wait = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 3;
    // 獲取提示框範圍
    var $myAlert = $('body #myAlert');
    // 如果提示框範圍並不存在於頁面之中，則進行創建並加入到body裏面。
    if (!$myAlert.length) {
      $myAlert = $('<div id="myAlert" class="my-alert"></div>');
      $('body').append($myAlert);
    }
    // 提示框
    var $alert = $("\n            <div class=\"alert alert-".concat(type, " fade alert-dismissible\" role=\"alert\">\n                <div data-bs-dismiss=\"alert\">").concat(msg, "</div>\n                <button type=\"button\" class=\"btn-close mt-1\" data-bs-dismiss=\"alert\" aria-label=\"Close\"></button>\n                <div class=\"progress\">\n                    <div class=\"progress-bar\" role=\"progressbar\" style=\"--speed: ").concat(wait, "s;\"></div>\n                </div>\n            </div>\n        "));
    // 將提示框加入到提示框範圍的最後一位。
    $myAlert.prepend($alert);
    // 用於效果延遲，如果沒有延遲的話，無法正確顯示出動畫效果。
    setTimeout(function () {
      $alert.addClass('show');

      // 顯示之後於多少秒關閉此提示框。
      wait && setTimeout(function () {
        bootstrap.Alert.getOrCreateInstance($alert.get(0)).close();
      }, wait * 1000);
    }, 50);
  }

  // 封裝 ajax 請求為 promise
  function _ajax(type, url, data) {
    url = _toUrl(url);
    return new Promise(function (resolve) {
      $.ajax({
        type: type,
        url: url,
        data: JSON.stringify(data),
        success: function success(data) {
          resolve(data);
        },
        contentType: 'application/json',
        dataType: 'json'
      }).fail(function (e) {
        resolve(e.responseJSON);
      });
    });
  }

  /**
   * 畫面請求數據時的過渡效果
   * @param {*} target 
   * @returns 
   */
  function _spinner(target) {
    var templateHtml = "\n            <span class=\"spinner-grow\" role=\"status\" aria-hidden=\"true\"></span>\n        ";
    var $target = _getTarget(target || 'body');
    if ($target.is('button, [type=button]')) {
      var $spinner = $(templateHtml);
      $spinner.addClass('spinner-grow-sm me-2');
      $target.prepend($spinner);
      return;
    }
  }

  /**
   * 關閉過度效果
   * @param {*} target 
   * @returns 
   */
  function _closeSpinner(target) {
    var $target = _getTarget(target || 'body');
    if ($target.is('button, [type=button]')) {
      $target.find('.spinner-grow').remove();
      return;
    }
  }
  /**
   * 獲取目標範圍的輸入資料。
   * @param {*} target 目標
   * @param {*} onlyChange 是否只獲取有修改的(需搭配initData一同使用)
   * @param {*} notEmpty 是否過慮空值
   * @returns 
   */
  function _getData(target, onlyChange) {
    var notEmpty = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var $target = _getTarget(target);
    if (!$target) {
      return null;
    }
    var data = {};
    $target.find(':input[name]:not(button, [type=button], [type=submit])').each(function () {
      if (onlyChange === true && $(this).attr('data-ori-value') == $(this).val()) {
        return;
      }
      if (notEmpty === true && !$(this).val()) {
        return;
      }
      data[this.name] = $(this).val();
    });
    return data;
  }

  /**
   * 獲取目標範圍的修改資料。
   * @param {*} target 
   * @param {Array<String>} keys 不倫是否修改，都會獲取的欄位。
   * @returns 
   */
  function _getChangeData(target) {
    var keys = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    var $target = _getTarget(target);
    if (!$target) {
      return null;
    }
    var data = {};
    $target.find(':input[name]:not(button, [type=button], [type=submit])').each(function () {
      if (!(keys || []).includes(this.name) && $(this).attr('data-ori-value') == $(this).val()) {
        return;
      }
      data[this.name] = $(this).val();
    });
    return data;
  }

  /**
   * 清除目標範圍的輸入資料
   * @param {*} target 
   * @returns 
   */
  function _clearData(target) {
    var $target = _getTarget(target);
    if (!$target) {
      return null;
    }
    var data = {};
    $target.find(':input[name]:not(button, [type=button], [type=submit])').each(function () {
      if ($(this).is('select')) {
        $(this).val(function () {
          return $(this).find(':first').val();
        });
        return;
      }
      if ($(this).is('[type=check], [type=radio]')) {
        $(this).prop('checked', false);
        return;
      }
      $(this).val(null);
    });
    return data;
  }

  /**
   * confirm
   * @param {*} message 
   * @returns 
   */
  function _confirm(message) {
    return new Promise(function (resolve) {
      bootbox.confirm({
        message: message,
        locale: navigator.language,
        callback: function callback(result) {
          resolve(result);
        }
      });
    });
  }

  /**
   * 初始化目標範圍的所有輸入筐，用於檢查是否有資料異動。
   * @param {*} target 
   * @param {*} data 
   */
  function _initData(target) {
    var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var $target = _getTarget(target);
    for (var _i = 0, _Object$entries = Object.entries(data); _i < _Object$entries.length; _i++) {
      var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
        field = _Object$entries$_i[0],
        value = _Object$entries$_i[1];
      $target.find("[name=\"".concat(field, "\"]")).val(value);
      $target.find("[name=\"".concat(field, "\"]")).attr('data-ori-value', value);
    }
  }

  /**
   * 欄位檢核
   * @param {*} target 
   * @returns 
   */
  function _checkValidity(target) {
    var $target = _getTarget(target);
    var result = $target.get(0).checkValidity();
    _clearValidity(target);
    if ($target.is('.needs-validation')) {
      $target.find(':input').each(function () {
        if (!this.validity.valid) {
          if (this.validity.valueMissing) {
            $(this).addClass('is-invalid');
            $(this).siblings('.invalid-feedback').text($(this).attr('data-require-msg'));
          } else if (this.validity.patternMismatch) {
            $(this).addClass('is-invalid');
            $(this).siblings('.invalid-feedback').text($(this).attr('data-pattern-msg'));
          } else if (this.validity.customError) {
            $(this).addClass('is-invalid');
            $(this).siblings('.invalid-feedback').text($(this).attr('data-custom-msg'));
          } else if (this.validity.tooShort) {
            $(this).addClass('is-invalid');
            $(this).siblings('.invalid-feedback').text($(this).attr('data-too-short-msg'));
          }
        } else if ($(this).is('[data-min-length]') && +$(this).attr('data-min-length') > $(this).val().length) {
          $(this).addClass('is-invalid');
          $(this).siblings('.invalid-feedback').text($(this).attr('data-too-short-msg'));
          result = false;
        }
      });
    }
    return result;
  }

  /**
   * 清除欄位檢核的訊息
   * @param {*} target 
   */
  function _clearValidity(target) {
    var $target = _getTarget(target);
    $target.find('.is-invalid').removeClass('is-invalid');
  }

  /**
   * 將欄位恢復為初始值
   * @param {*} target 
   * @returns 
   */
  function _resetValueToOri(target) {
    var $target = _getTarget(target);
    if ($target.is(':input')) {
      $target.val(function () {
        return $(this).attr('data-ori-value') || '';
      });
      return;
    }
    $target.find(':input[data-ori-value]').val(function () {
      return $(this).attr('data-ori-value') || '';
    });
  }

  /**
   * 檔案下載
   * @param {*} url 
   * @returns 
   */
  function _download(url) {
    url = _toUrl(url);
    return new Promise(function (resolve) {
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function () {
        if (this.readyState === 4) {
          if (this.status === 200) {
            var filename = "";
            var disposition = xhr.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
              var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
              var matches = filenameRegex.exec(disposition);
              if (matches != null && matches[1]) {
                filename = matches[1].replace(/['"]/g, '');
              }
            }
            saveAs(this.response, decodeURI(filename));
            resolve({
              success: true
            });
          } else {
            var reader = new FileReader();
            reader.onload = function () {
              resolve(JSON.parse(reader.result));
            };
            reader.readAsText(this.response);
          }
        }
      };
      xhr.open('GET', url);
      xhr.responseType = 'blob';
      xhr.send();
    });
  }

  /**
   * 前往對應網址
   * @param {*} url 
   * @param {*} isReplace 
   */
  function _goto(url) {
    var isReplace = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    url = _toUrl(url);
    if (isReplace) {
      window.location.replace(url);
    } else {
      window.location.href = url;
    }
  }

  /**
   * 上傳檔案
   * @param {*} url 
   * @param {*} file 
   * @returns 
   */
  function _upload(url, file) {
    url = _toUrl(url);
    var formData = new FormData();
    formData.set('file', file);
    return new Promise(function (resolve) {
      $.ajax({
        type: 'POST',
        url: url,
        data: formData,
        contentType: false,
        processData: false,
        success: function success(data) {
          resolve(data);
        }
      }).fail(function (e) {
        resolve(e.responseJSON);
      });
    });
  }

  /**
   * 為確保網址的正確性，在此判斷是否有加上前綴http，
   * 如果沒有則需要自行帶入當前系統的基礎網址。
   * @param {*} url 
   * @returns 
   */
  function _toUrl(url) {
    if (url && !url.startsWith('http')) {
      url = baseUrl + url;
    }
    return url;
  }

  /**
   * 複製文字
   * @param {*} text 
   */
  function _copyText(text) {
    var el = document.createElement('textarea');
    el.value = text;
    document.body.appendChild(el);
    el.select();
    el.focus();
    document.execCommand('copy');
    document.body.removeChild(el);
  }

  /**
   * 等待/睡眠
   * @param {*} time 毫秒
   * @returns 
   */
  function _sleep(time) {
    return new Promise(function (resolve) {
      setTimeout(function () {
        return resolve();
      }, time);
    });
  }
  window.util = _objectSpread(_objectSpread({}, window.util), {}, {
    confirm: function confirm(msg) {
      return _confirm(msg);
    },
    form: {
      getData: function getData(target, onlyChange, notEmpty) {
        return _getData(target, onlyChange, notEmpty);
      },
      getChangeData: function getChangeData(target, keys) {
        return _getChangeData(target, keys);
      },
      clearData: function clearData(target) {
        return _clearData(target);
      },
      initData: function initData(target, data) {
        return _initData(target, data);
      },
      checkValidity: function checkValidity(target) {
        return _checkValidity(target);
      },
      clearValidity: function clearValidity(target) {
        return _clearValidity(target);
      },
      resetValueToOri: function resetValueToOri(target) {
        return _resetValueToOri(target);
      }
    },
    post: function post(url, data) {
      return _ajax('POST', url, data);
    },
    get: function get(url, data) {
      return _ajax('GET', url, data);
    },
    put: function put(url, data) {
      return _ajax('PUT', url, data);
    },
    "delete": function _delete(url, data) {
      return _ajax('DELETE', url, data);
    },
    upload: function upload(url, file) {
      return _upload(url, file);
    },
    download: function download(url) {
      return _download(url);
    },
    showSuccess: function showSuccess(msg, wait) {
      _showMsg('success', msg, wait);
    },
    showError: function showError(msg, wait) {
      _showMsg('danger', msg, wait);
    },
    showWarning: function showWarning(msg, wait) {
      _showMsg('warning', msg, wait);
    },
    spinner: {
      show: function show(target) {
        return _spinner(target);
      },
      close: function close(target) {
        return _closeSpinner(target);
      }
    },
    location: {
      href: function href(url) {
        return _goto(url);
      },
      replace: function replace(url) {
        return _goto(url, true);
      }
    },
    toUrl: function toUrl(url) {
      return _toUrl(url);
    },
    copyText: function copyText(text) {
      return _copyText(text);
    },
    sleep: function sleep(time) {
      return _sleep(time);
    }
  });
});
/******/ })()
;