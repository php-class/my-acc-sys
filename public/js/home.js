/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!******************************!*\
  !*** ./resources/js/home.js ***!
  \******************************/
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
$(function () {
  Chart.register(ChartDataLabels);
  var pieOptions = {
    plugins: {
      title: {
        display: true
      },
      legend: {
        position: "bottom"
      },
      datalabels: {
        formatter: function formatter(value, ctx) {
          var sum = 0;
          var dataArr = ctx.chart.data.datasets[0].data;
          dataArr.map(function (data) {
            sum += +data;
          });
          var percentage = (+value * 100 / sum).toFixed(2) + "%";
          return percentage;
        },
        color: "#fff"
      }
    }
  };
  var statisticsChart = initStatisticsChart();
  util.get("/api/expenditure/statistics").then(function (result) {
    var _chart$data$datasets;
    if (!result.success) {
      return util.showError(result.msg);
    }
    var chart = statisticsChart.chart,
      data = statisticsChart.data,
      datasets = statisticsChart.datasets;
    var dataset = datasets['TOTAL'];
    dataset['amount'] = _objectSpread({}, data);
    var _iterator = _createForOfIteratorHelper(result.data),
      _step;
    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var _step$value = _step.value,
          amount = _step$value.amount,
          date = _step$value.date,
          _type = _step$value.type;
        if (!datasets[_type]) {
          datasets[_type] = {
            label: _type,
            amount: _objectSpread({}, data)
          };
        }
        if (typeof data[date] !== 'undefined') {
          dataset['amount'][date] += +amount;
          datasets[_type]['amount'][date] += +amount;
        }
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
    for (var type in datasets) {
      datasets[type]['data'] = Object.values(datasets[type]['amount']);
    }
    (_chart$data$datasets = chart.data.datasets).push.apply(_chart$data$datasets, _toConsumableArray(Object.values(datasets)));
    chart.update();
  });
  util.get("/api/expenditure/statistics/type").then(function (result) {
    if (!result.success) {
      return util.showError(result.msg);
    }
    var labels = [];
    var data = [];
    var _iterator2 = _createForOfIteratorHelper(result.data),
      _step2;
    try {
      for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
        var _step2$value = _step2.value,
          name = _step2$value.name,
          count = _step2$value.count;
        labels.push(name);
        data.push(count);
      }
    } catch (err) {
      _iterator2.e(err);
    } finally {
      _iterator2.f();
    }
    var $ctx = $("#statisticsByType");
    new Chart($ctx.get(0), {
      type: "pie",
      data: {
        labels: labels,
        datasets: [{
          label: $ctx.attr("data-label"),
          data: data,
          hoverOffset: 4
        }]
      },
      options: _objectSpread(_objectSpread({}, pieOptions), {}, {
        plugins: _objectSpread(_objectSpread({}, pieOptions.plugins), {}, {
          title: {
            display: true,
            text: $ctx.attr("data-title")
          }
        })
      })
    });
  });
  util.get("/api/expenditure/statistics/payment").then(function (result) {
    if (!result.success) {
      return util.showError(result.msg);
    }
    var labels = [];
    var data = [];
    var _iterator3 = _createForOfIteratorHelper(result.data),
      _step3;
    try {
      for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
        var _step3$value = _step3.value,
          name = _step3$value.name,
          count = _step3$value.count;
        labels.push(name);
        data.push(count);
      }
    } catch (err) {
      _iterator3.e(err);
    } finally {
      _iterator3.f();
    }
    var $ctx = $("#statisticsByPaymentMethod");
    new Chart($ctx.get(0), {
      type: "pie",
      data: {
        labels: labels,
        datasets: [{
          label: $ctx.attr("data-label"),
          data: data,
          hoverOffset: 4
        }]
      },
      options: _objectSpread(_objectSpread({}, pieOptions), {}, {
        plugins: _objectSpread(_objectSpread({}, pieOptions.plugins), {}, {
          title: {
            display: true,
            text: $ctx.attr("data-title")
          }
        })
      })
    });
  });
  function initStatisticsChart() {
    var $ctx = $("#statistics");
    var labels = {};
    var data = {};
    var now = moment();
    var last = moment().subtract(1, 'M');
    var diff = now.diff(last, 'd');
    labels[last.format('YYYY-MM-DD')] = last.format('MM/DD');
    data[last.format('YYYY-MM-DD')] = 0;
    for (var idx = 0; idx < diff; idx++) {
      var day = last.add(1, 'd');
      labels[day.format('YYYY-MM-DD')] = day.format('MM/DD');
      data[day.format('YYYY-MM-DD')] = 0;
    }
    var datasets = {
      'TOTAL': {
        label: $ctx.attr('data-label'),
        data: Object.values(_objectSpread({}, data))
      }
    };
    var statisticsChart = new Chart($ctx.get(0), {
      type: "line",
      data: {
        labels: Object.values(labels),
        datasets: Object.values(datasets)
      },
      options: {
        maintainAspectRatio: false,
        responsive: true,
        plugins: {
          title: {
            display: true,
            text: $ctx.attr('data-title')
          },
          colors: {
            forceOverride: true
          }
        },
        interaction: {
          intersect: false
        },
        scales: {
          x: {
            display: true,
            title: {
              display: true,
              text: $ctx.attr('data-x-label')
            }
          },
          y: {
            display: true,
            title: {
              display: true,
              text: $ctx.attr('data-y-label')
            },
            suggestedMin: 0
          }
        }
      }
    });
    return {
      chart: statisticsChart,
      labels: labels,
      data: data,
      datasets: datasets
    };
  }
});
/******/ })()
;