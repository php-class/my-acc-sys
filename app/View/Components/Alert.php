<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Alert extends Component
{
    /**
     * The alert type.
     *
     * @var string
     */
    public $type;
 
    /**
     * The alert message.
     *
     * @var string
     */
    public $message;

    public $class;
 
    /**
     * Create the component instance.
     *
     * @param  string  $type
     * @param  string  $message
     * @param  string  $class
     * @return void
     */
    public function __construct($type = null, $message = null, $class = null)
    {
        $this->type = $type;
        $this->message = $message;
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.alert');
    }
}
