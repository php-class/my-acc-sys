<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function success($result = null) {
        $msg = null;
        $data = null;
        $total = null;

        if (is_object($result)) {
            $data = $result;
        } else if (is_array($result)) {
            $data = $result['data'] ?? ($result[0] ?? null);
            $total = $result['total'] ?? ($result[1] ?? null);
            $msg = $result['msg'] ?? ($result[2] ?? null);
        } else {
            $msg = $result;
        }

        return response()->json([
            'success' => true,
            'data' => $data,
            'total' => $total,
            'msg' => $msg,
        ]);
    }

    protected function fail(?string $msg = '', ?int $status = 500) {
        return response()->json([
            'success' => false,
            'msg' => $msg,
        ]);
    }

}
