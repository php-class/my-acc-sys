<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * 獲取當前用戶的個人訊息。
     */
    public function get(Request $request) {
        return $this->success([
            $request->user()
        ]);
    }

    /**
     * 更新用戶資料
     */
    public function update(Request $request) {
        $user = UserModel::find($request->user()->id);
            
        if ($request->has('last_name')) {
            $user->last_name = $request->input('last_name');
        }

        if ($request->has('first_name')) {
            $user->first_name = $request->input('first_name');
        }

        if ($request->has('birthday')) {
            $user->birthday = $request->input('birthday');
        }

        if ($request->has('avatar')) {
            $user->avatar = $request->input('avatar');
        }

        $user->save();

        Auth::setUser($user);

        return $this->success(__('success.modify'));
    }

    /**
     * 修改密碼
     */
    public function updatePassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'new_password' => ['required', 'regex:/^((?=.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*|(?=.{8,}$)(?=.*\d)(?=.*[a-zA-Z]).*)/i'],
            'confirm_password' => 'required',
        ], [
            'password.required' => __('require.password'),
            'password.new_password' => __('require.new.password'),
            'password.confirm_password' => __('require.new.confirm.password'),
            'password.regex' => __('pattern.error.password'),
        ]);

        if ($validator->fails()) {
            return abort(400, $validator->errors()->first());
        }

        // 獲取傳入參數
        $password = $request->input('password');
        $newPassword = $request->input('new_password');
        $confirmPassword = $request->input('confirm_password');
        
        $user = UserModel::find($request->user()->id);

        // 檢查原密碼是否輸入正確
        if (!Hash::check($password, $user->password)) {
            return abort(400, __('error.ori.password'));
        }

        // 檢查新密碼與確認密碼是否相同
        if ($newPassword !== $confirmPassword) {
            return abort(400, __('password.not.same'));
        }

        // 檢查新密碼與原密碼是否不同
        if (Hash::check($newPassword, $user->password)) {
            return abort(400, __('password.same'));
        }
        
        // 更新密碼
        $user->password = Hash::make($newPassword);
        $user->save();

        return $this->success();
    }
}
