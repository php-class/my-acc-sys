<?php

namespace App\Http\Controllers\Development;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class DBController extends Controller
{
    /**
     * 創建資料庫以及新增基本數據
     */
    public function index(Request $request) {
        try {
            $db_connection = env('DB_CONNECTION');
            $db_host = env('DB_HOST');
            $db_port = env('DB_PORT');
            $db_database = env('DB_DATABASE');
            $db_username = env('DB_USERNAME');
            $db_password = env('DB_PASSWORD');

            $this->createDb(
                $db_connection,
                $db_host,
                $db_port,
                $db_username,
                $db_password,
                $db_database
            );

            echo "資料庫創建成功<br/>";
            Artisan::call('migrate');
            echo "資料表創建成功<br/>";
            Artisan::call('db:seed');
            echo "資料創建成功<br/>";
        } catch (\Throwable $e) {
            echo "發生異常：" . $e->getMessage();
        }
        exit();
    }

    private function createDb($connection, $host, $port, $username, $password, $database) {
        $pdo = new \PDO($connection .':host=' . $host . ';port=' . $port . ';', $username, $password);
        $pdo->exec('CREATE DATABASE IF NOT EXISTS ' . $database);
    }
}