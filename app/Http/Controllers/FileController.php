<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FileModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    /**
     * 根據 uuid 獲取檔案
     */
    public function get(Request $request, $uuid) {
        // 透過 uuid 獲取檔案
        $file = FileModel::where('uuid', $uuid)->first();
        // 檢查檔案是否存在，如果不存在則返回錯誤結果。
        if (!$file) {
            return $this->fail(__('data.not.found'), 404);
        }

        return Storage::download($file->path, $file->name, [ 'Content-Disposition' => 'inline' ]);
    }

    /**
     * 上傳檔案
     */
    public function upload(Request $request) {
        $user = $request->user();
        // 獲取上傳檔案
        $file = $request->file('file');
        // 將檔案儲存至 uploads 中，且返回儲存路徑。
        $path = $file->store('public');
        $uuid = DB::selectOne('SELECT UUID() AS id')->id;

        // 新增此紀錄至資料庫中
        FileModel::create([
            'name' => $file->getClientOriginalName(),
            'type' => $file->getClientMimeType(),
            'size' => $file->getSize(),
            'path' => $path,
            'created_at' => $user->id,
            'uuid' => $uuid,
        ]);

        // 將uuid做為結果返回。
        return $this->success([ [ 'file_id' => $uuid ] ]);
    }

    /**
     * 刪除檔案
     */
    public function delete($uuid) {
        // 獲取 FileModel 實例
        // 獲取檔案紀錄
        $file = FileModel::where('uuid', $uuid)->first();
        // 判斷檔案是否存在，如果不在則返回錯誤結果。
        if (!$file) {
            return abort(404, __('data.not.found'));
        }
        // 針對實體檔案進行刪除。
        if (Storage::exists($file->path)) {
            Storage::delete($file->path);
        }

        // 針對檔案紀錄進行刪除。
        $file->delete();

        return $this->success(__('success.delete'));
    }
}
