<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\UserModel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    /**
     * 登入
     */
    public function login(Request $request) {
        // 取得用戶ip
        $ip = Str::replace('::1', 'localhost', $request->ip());
        // 判斷是否存在登入錯誤過多紀錄
        // 有的話則返回登入過多提示
        if (Cache::has('login.fail.' . $ip)) {
            return abort(400, __('login.fail.overtime'));
        }
        // 驗證是否有正確輸入帳密
        $validator = Validator::make($request->all(), [
            'account' => ['required'],
            'password' => ['required'],
        ], [
            'account.required' => __('require.account'),
            'password.required' => __('require.password'),
        ]);

        if ($validator->fails()) {
            return abort(400, $validator->errors()->first());
        }
        // 獲取登入錯誤次數
        $loginCount = $request->session()->get('login_count', 0);

        $data = $request->all(['account', 'password']);
        $data['is_active'] = true;
        // 進行登入
        if (Auth::attempt($data)) {
            $request->session()->forget('login_count');

            $request->session()->regenerate();
            
            UserModel::where('id', Auth::user()->id)
                ->update([ 'last_login_at' => now() ]);

            return $this->success();
        }

        $loginCount++;
        $request->session()->put('login_count', $loginCount);
        // 如果登入次數錯誤超過五次，則進行封鎖12小時。
        if ($loginCount >= 5) {
            Cache::put('login.fail.' . $ip, now(), 12 * 60 * 60);
            $request->session()->forget('login_count');
        }

        return abort(400, __('login.fail'));
    }

    /**
     * 註冊
     */
    public function register(Request $request) {
        // 驗證
        $validator = Validator::make($request->all(), [
            'account' => ['required', 'min:8', 'unique:sys_users'],
            'password' => ['required', 'regex:/^((?=.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*|(?=.{8,}$)(?=.*\d)(?=.*[a-zA-Z]).*)/i', 'confirmed'],
        ], [
            'account.required' => __('require.account'),
            'account.min' => __('min.length.account'),
            'account.unique' => __('exists.account'),
            'password.required' => __('require.password'),
            'password.regex' => __('pattern.error.password'),
            'password.confirmed' => __('password.not.same'),
        ]);

        if ($validator->fails()) {
            return abort(400, $validator->errors()->first());
        }
        // 檢查帳號是否已存在，如果存在則返回錯誤提示。
        $count = UserModel::where('account', $request->input('account'))->count();

        if ($count) {
            return abort(400, __('exists.account'));
        }
        // 創建用戶。
        UserModel::create([
            'account' => $request->input('account'),
            'password' => Hash::make($request->input('password'))
        ]);

        return $this->success();
    }

    /**
     * 登出
     */
    public function logout(Request $request) {
        Auth::logout();
 
        $request->session()->invalidate();
    
        $request->session()->regenerateToken();

        return redirect('login');
    }
}
