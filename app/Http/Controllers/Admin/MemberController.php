<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class MemberController extends Controller
{
    /**
     * 獲取會員資料
     */
    public function get(Request $request) {
        $limit = $request->input('limit', 10);
        $offset = $request->input('offset', 0);

        $builder = UserModel::select();

        if ($request->has('role')) {
            $builder->where('role', $request->input('role'));
        }

        if ($request->has('is_active')) {
            $builder->where('is_active', $request->input('is_active'));
        }

        if ($request->has('date_fm')) {
            $builder->where('last_login_at', '>=', $$request->input('date_fm') . " 00:00:00");
        }

        if ($request->has('date_to')) {
            $builder->where('last_login_at', '<=', $$request->input('date_to') . " 23:59:59.999");
        }

        $count = $builder->count();

        $result = $builder
            ->orderBy('id', 'asc')
            ->limit($limit)
            ->offset($offset)
            ->get();

        return $this->success([$result, $count]);
    }

    /**
     * 獲取單筆會員資料
     */
    public function getOne(Request $request, $id) {
        $user = UserModel::find($id);
        
        // 檢查資料是否存在，如果不存在則返回錯誤結果。
        if (!$user) {
            return abort(404, __('data.not.found'));
        }

        return $this->success($user);
    }

    /**
     * 更新會員資料
     */
    public function update(Request $request, $id) {
        $user = UserModel::find($id);
    
        if (!$user) {
            return abort(404, __('data.not.found'));
        }

        if ($request->has('role')) {
            $user->role = $request->input('role');
        }

        if ($request->has('is_active')) {
            $user->is_active = $request->input('is_active');
        }

        $user->save();

        return $this->success(__('success.modify'));
    }

    /**
     * 產生會員新密碼
     */
    public function generatePassword(int $id) {
        $user = UserModel::find($id);

        if (!$user) {
            return abort(404, __('data.not.found'));
        }

        $newPassword = 'Aa123456';

        $user->password = Hash::make($newPassword);

        $user->save();

        return $this->success([ ['password' => $newPassword ] ]);
    }
}
