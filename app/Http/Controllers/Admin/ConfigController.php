<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ConfigModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ConfigController extends Controller
{
    /**
     * 獲取配置
     */
    public function get(Request $request) {
        // 獲取顯示的資料數量
        $limit = $request->input('limit', 10);
        // 獲取所顯示的頁碼
        $offset = $request->input('offset', 0);

        $builder = ConfigModel::select();
        
        if ($request->has('code')) {
            $builder->where('UPPER(code)', 'like' , strtoupper($request->input('code')) . '%');
        }

        if ($request->has('type')) {
            $builder->where('type', $request->input('type'));
        }

        if ($request->has('is_active')) {
            $builder->where('is_active', $$request->input('is_active'));
        }

        $count = $builder->count();

        $result = $builder
            ->orderBy('type', 'asc')
            ->orderBy('id', 'asc')
            ->limit($limit)
            ->offset($offset)
            ->get();

        return $this->success([ $result, $count ]);
    }

    /**
     * 獲取配置類型，提供查詢下拉列表使用
     */
    public function getTypes(Request $request) {
        $result = ConfigModel::distinct()->pluck('type');

        return $this->success([ $result ]);
    }

    /**
     * 新增或修改配置
     */
    public function save(Request $request) {

        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'type' => 'required',
            'desc' => 'required',
        ], [
            'code.required' => __('require.code'),
            'type.required' => __('require.type'),
            'desc.required' => __('require.desc'),
        ]);

        if ($validator->fails()) {
            return abort(400, $validator->errors()->first());
        }

        $user = $request->user();
    
        $id = $request->input('id');
        $data = $request->all(['code', 'type', 'desc', 'is_active']);

        $code = $data['code'];
        $type = $data['type'];

        $count = ConfigModel::where('code', $code)
            ->where('type', $type)
            ->where('id', '!=', $id)
            ->count();

        if ($count > 0)  {
            return abort(400, __('config.exists'));
        }

        if ($request->missing('id')) {
            $data['created_by'] = $user->id;
            ConfigModel::create($data);
            return $this->success(__('success.add'));
        }

        $config = ConfigModel::find($id);

        if (!$config) {
            return abort(404, __('data.not.found'));
        }

        $data['updated_by'] = $user->id;
        $config->update($data);

        return $this->success(__('success.modify'));
    }

    /**
     * 獲取單筆配置紀錄
     */
    public function getOne(Request $request, $id) {
        $config = ConfigModel::find($id);

        // 檢查資料是否存在，如果不存在則返回錯誤結果。
        if (!$config) {
            return abort(404, __('data.not.found'));
        }

        return $this->success($config);
    }

    /**
     * 刪除配置
     */
    public function delete(Request $request) {
        $rows = $request->collect('rows');
    
        ConfigModel::destroy($rows);

        return $this->success(__('success.delete'));
    }
}
