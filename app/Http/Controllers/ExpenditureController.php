<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ExpenditureModel;
use App\Models\ConfigModel;
use App\Constants\ConfigType;
use Illuminate\Support\Facades\DB;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExpenditureController extends Controller
{
    /**
     * 獲取支出紀錄
     */
    public function get(Request $request)
    {
        // 獲取當前用戶
        $user = $request->user();
        // 獲取顯示的資料數量
        $limit = $request->input('limit', 10);
        // 獲取所顯示的頁碼
        $offset = $request->input('offset', 0);

        $builder = ExpenditureModel::where('sys_expenditures.created_by', $user->id);
        
        // 付款方式
        if ($request->has('payment_method')) {
            $builder->where('payment_method', $request->input('payment_method'));
        }

        // 支出類型
        if ($request->has('type')) {
            $builder->where('sys_expenditures.type', $request->input('type'));
        }

        // 支出日期起日
        if ($request->has('dateFm')) {
            $builder->where('date', '>=', $request->input('dateFm'));
        }

        // 支出日期迄日
        if ($request->has('dateTo')) {
            $builder->where('date', '<=', $request->input('dateTo'));
        }

        // 獲取比數
        $count = $builder->count();
        // 獲取支出紀錄
        $expenditures = $builder
            ->select(
                'sys_expenditures.*',
                'types.desc as type_desc',
                'types.code as type_code',
                'pm.desc as payment_method_desc',
                'pm.code as payment_method_code',
            )
            ->leftJoin('sys_configs as types', 'types.code', '=', 'sys_expenditures.type')
            ->leftJoin('sys_configs as pm', 'pm.code', '=', 'sys_expenditures.payment_method')
            ->orderBy('created_at', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get();
        
        return $this->success([ $expenditures, $count ]);
    }

    /**
     * 從配置獲取支出類型
     */
    public function getTypes(Request $request) 
    {

        $builder = ConfigModel::where('is_active', true)
            ->where('type', ConfigType::EXPENDITURE_TYPE);

        // 類型數量
        $count = $builder->count();
        // 類型資料
        $types = $builder->get();

        return $this->success([ $types, $count ]);
    }

    /**
     * 從配置中獲取付款方式
     */
    public function getPaymentMethods(Request $request) 
    {
        $builder = ConfigModel::where('is_active', true)
            ->where('type', ConfigType::EXPENDITURE_PAYMENT_METHOD);

        // 付款方式數量
        $count = $builder->count();

        // 付款方式資料
        $paymentMethods = $builder->get();

        return $this->success([ $paymentMethods, $count ]);
    }

    /**
     * 獲取單筆支出紀錄
     */
    public function getOne(Request $request, $id) {
        // 根據支出id獲取紀錄
        $expenditure = ExpenditureModel::find($id);

        // 檢查資料是否存在，如果不存在則返回錯誤結果。
        if (!$expenditure) {
            return abort(404, __('data.not.found'));
        }

        return $this->success($expenditure);
    }

    /**
     * 新增或修改支出紀錄
     */
    public function save(Request $request) {
        $user = $request->user();
        // 獲取請求參數 id
        $id = $request->input('id');
        // 獲取待更新或創建資料
        $data = $request->only([ 'date', 'payment_method', 'type', 'amount', 'remark' ]);

        if ($request->missing('id')) {
            $data['created_by'] = $user->id;
            // 創建支出紀錄
            ExpenditureModel::create($data);

            return $this->success([$data, null, __('success.add')]);
        }

        // 根據 id 獲取當前的支出紀錄。
        $expenditure = ExpenditureModel::find($id);
        // 判斷紀錄是否存在，如果不在則返回錯誤結果，必且停止更新。
        if (!$expenditure) {
            return abort(404);
        }

        $data['updated_by'] = $user->id;

        $expenditure->update($data);
        
        return $this->success([ $data, null, __('success.modify') ]);
    }

    /**
     * 刪除支出紀錄
     */
    public function delete(Request $request) {
        // 獲取預刪除的紀錄資料。
        $rows = $request->input('rows');
        // 進行刪除
        ExpenditureModel::destroy($rows);

        return $this->success(__('success.delete'));
    }

    /**
     * 統計近一個月的各個支出類型數量
     */
    public function getStatisticsByType(Request $request) {
        $user = $request->user();
        
        $now = date('Y-m-d');
        $last = date('Y-m-d', strtotime('-1 month'));

        $result = DB::select('
            SELECT 
                IFNULL((SELECT COUNT(*) 
                    FROM sys_expenditures 
                    WHERE sys_expenditures.type = sys_configs.code
                    AND sys_expenditures.created_by = ?
                    AND date <= ?
                    AND date >= ?
                ), 0) `count`,
                sys_configs.desc `name`
            FROM sys_configs
            WHERE type = ? 
        ', [
            $user->id,
            $now,
            $last,
            ConfigType::EXPENDITURE_TYPE,
        ]);

        return $this->success([ $result ]);
    }

    /**
     *  統計近一個月的各個付款方式數量
     */
    public function getStatisticsByPaymentMethod(Request $request) {
        $user = $request->user();
        $now = date('Y-m-d');
        $last = date('Y-m-d', strtotime('-1 month'));
        
        $result = DB::select('
            SELECT 
                IFNULL((SELECT COUNT(*) 
                    FROM sys_expenditures 
                    WHERE sys_expenditures.payment_method = sys_configs.code
                    AND sys_expenditures.created_by = ?
                    AND date <= ?
                    AND date >= ?
                ), 0) `count`,
                sys_configs.desc `name`
            FROM sys_configs
            WHERE type = ? 
        ', [
            $user->id,
            $now,
            $last,
            ConfigType::EXPENDITURE_PAYMENT_METHOD,
        ]);

        return $this->success([ $result ]);
    }

    /**
     * 根據支出類型統計近一個月的每日支出金額
     */
    public function getStatistics(Request $request) {
        $user = $request->user();
        $now = date('Y-m-d');
        $last = date('Y-m-d', strtotime('-1 month'));
        $result = DB::table('sys_expenditures')
            ->selectRaw('SUM(amount) as amount, DATE_FORMAT(date, "%Y-%m-%d") as date, sys_configs.desc as type')
            ->leftJoin('sys_configs', function($join) {
                $join->on('sys_configs.code', '=', 'sys_expenditures.type')
                    ->where('sys_configs.type', '=', DB::raw('"' . ConfigType::EXPENDITURE_TYPE . '"'));
            })
            ->where('sys_expenditures.created_by', $user->id)
            ->where('date', '<=', $now)
            ->where('date', '>=', $last)
            ->groupByRaw('DATE_FORMAT(date, "%Y-%m-%d"), sys_configs.type, sys_configs.desc')
            ->get();
        
        return $this->success([ $result ]);
    }

    /**
     * 根據年月導出excel
     */
    public function export(Request $request, $date) {
        $user = $request->user();

        // 檔案名稱
        $file_name = $date . __('expenditure') . '.xlsx';
        // 引用第三方庫來創建excel檔案
        $spreadsheet = new Spreadsheet();
        // 獲取當前sheet
        $sheet = $spreadsheet->getActiveSheet();
        $idx = 1;
        // 寫入title
        $sheet->setCellValue('A' . $idx, __('date'));
        $sheet->setCellValue('B' . $idx, __('payment.method'));
        $sheet->setCellValue('C' . $idx, __('expenditure.type'));
        $sheet->setCellValue('D' . $idx, __('amount'));
        $sheet->setCellValue('E' . $idx, __('remark'));
        // 獲取支出紀錄
        $result = ExpenditureModel::select(
                'date',
                'types.desc as type',
                'payment.desc as payment_method',
                'amount',
                'remark'
            )->leftJoin('sys_configs as types', function($join) {
                $join->on('types.code', '=', 'sys_expenditures.type')
                    ->where('types.type', '=', DB::raw('"' . ConfigType::EXPENDITURE_TYPE . '"'));
            })->leftJoin('sys_configs as payment', function($join) {
                $join->on('payment.code', '=', 'sys_expenditures.payment_method')
                    ->where('payment.type', '=', DB::raw('"' . ConfigType::EXPENDITURE_PAYMENT_METHOD . '"'));
            })
            ->where('sys_expenditures.created_by', $user->id)
            ->where('date', '>=', str_replace("-", "", $date) . "01")
            ->where('date', '<=', str_replace("-", "", $date) . "31")
            ->orderBy('date', 'asc')
            ->get();
        // 支出總金額
        $total = 0;
        // 寫入每筆紀錄
        foreach($result as $row) {
            $idx++;
            $sheet->setCellValue('A' . $idx, $row->date);
            $sheet->setCellValue('B' . $idx, $row->payment_method);
            $sheet->setCellValue('C' . $idx, $row->type);
            $sheet->setCellValue('D' . $idx, $row->amount);
            $sheet->setCellValue('E' . $idx, $row->remark);

            $total += (int) $row->amount;
        }

        // 寫入總金額
        $sheet->setCellValue('C' . ($idx + 1), __('total.amount'));
        $sheet->setCellValue('D' . ($idx + 1), $total);

        // 以下為結果導出
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

        header("Content-Type: application/vnd.ms-excel");
        
        header('Content-Disposition: attachment; filename="' . urlencode($file_name) . '"');
        
        header('Cache-Control: max-age=0');

        $writer->save('php://output');

        exit;
    }
}
