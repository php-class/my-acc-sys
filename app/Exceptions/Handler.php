<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;
use Illuminate\Support\Arr;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {

        // 如果是在請求api時發，透過abort拋出異常的話，
        // 我們需要以json個結果格式返回。
        $this->renderable(function (HttpException $e, $request) {
            if ($request->is('api/*')) {
                return response()->json([
                    'msg' => empty($e->getMessage()) ? Response::$statusTexts[$e->getStatusCode()] : $e->getMessage(),
                    'success' => false,
                ], $e->getStatusCode());
            }
        });

        // 如果是在請求api時發，發現非預期的系統異常，
        // 我們需要以json個結果格式返回。
        $this->renderable(function (Throwable $e, $request) {
            if ($request->is('api/*')) {
                return response()->json([
                    'msg' => $e->getMessage() ?? '',
                    'success' => false,
                ], 500);
            }
        });
    }
}
