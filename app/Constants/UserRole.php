<?php
    namespace App\Constants;

    /**
     * 用戶表格中的權限種類
     */
    interface UserRole {
        /**
         * 管理人員
         */
        const ADMIN = "ADMIN";
        /**
         * 一般使用者
         */
        const GENERAL = "GENERAL"; 
    }