<?php
    namespace App\Constants;

    interface ConfigType {
        public const EXPENDITURE_TYPE = 'EXPENDITURE_TYPE';
        public const EXPENDITURE_PAYMENT_METHOD = 'EXPENDITURE_PAYMENT_METHOD';
    }