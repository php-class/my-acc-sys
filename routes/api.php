<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ExpenditureController;
use App\Http\Controllers\Admin\ConfigController;
use App\Http\Controllers\Admin\MemberController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\FileController;

Route::prefix('admin')->group(function() {
    Route::prefix('config')->group(function() {
        Route::get('/', [ConfigController::class, 'get']);
        Route::get('/type', [ConfigController::class, 'getTypes']);
        Route::get('/{id}', [ConfigController::class, 'getOne']);
        Route::post('/', [ConfigController::class, 'save']);
        Route::delete('/', [ConfigController::class, 'delete']);
    });

    Route::prefix('member')->group(function() {
        Route::get('/', [MemberController::class, 'get']);
        Route::get('/{id}', [MemberController::class, 'getOne']);
        Route::put('/{id}', [MemberController::class, 'update']);
        // Route::put('/generatePassword/{id}', [MemberController::class, 'generatePassword']);
    });
});

Route::withoutMiddleware('auth.api')->group(function() {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
});

Route::any('/logout', [AuthController::class, 'logout']);

Route::prefix('expenditure')->group(function() {
    Route::get('/', [ExpenditureController::class, 'get']);
    Route::get('/type', [ExpenditureController::class, 'getTypes']);
    Route::get('/payment', [ExpenditureController::class, 'getPaymentMethods']);
    Route::get('/statistics', [ExpenditureController::class, 'getStatistics']);
    Route::get('/statistics/type', [ExpenditureController::class, 'getStatisticsByType']);
    Route::get('/statistics/payment', [ExpenditureController::class, 'getStatisticsByPaymentMethod']);
    Route::post('/', [ExpenditureController::class, 'save']);
    Route::delete('/', [ExpenditureController::class, 'delete']);
    Route::get('/export/{date}', [ExpenditureController::class, 'export']);
    Route::get('/{id}', [ExpenditureController::class, 'getOne']);
});

Route::prefix('profile')->group(function() {
    Route::get('/', [ProfileController::class, 'get']);
    Route::put('/', [ProfileController::class, 'update']);
    Route::put('/reset', [ProfileController::class, 'updatePassword']);
});

Route::prefix('file')->group(function() {
    Route::get('/{uuid}', [FileController::class, 'get']);
    Route::post('/', [FileController::class, 'upload']);
    Route::delete('/{uuid}', [FileController::class, 'delete']);
});