<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Development\DBController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::view('/', 'home');
Route::view('/login', 'login')->withoutMiddleware('auth');
if (app()->environment() === 'local') {
    Route::any('/db', [DBController::class, 'index'])
        ->withoutMiddleware('auth');
}

Route::prefix('/admin')->group(function() {
    Route::fallback(function(Request $request) {
        $view = Str::replace('/', '_', Str::replace('admin/', '', $request->path()));
        
        if (View::exists('admin.' . $view)) {
            return view('admin.' . $view);
        }
    
        abort(404);
    });
});

Route::fallback(function(Request $request) {
    $view = Str::replace('/', '_', $request->path());

    if (View::exists($view)) {
        return view($view);
    }

    abort(404);
});