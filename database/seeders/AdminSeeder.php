<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Constants\UserRole;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = DB::table('sys_users')->where('account', 'admin')->count();
        if ($count === 0) {
            DB::table('sys_users')->upsert([[
                'account' => 'admin',
                'password' => Hash::make('admin'),
                'role' => UserRole::ADMIN,
            ]], [ 'account' ]);
        }
    }
}
