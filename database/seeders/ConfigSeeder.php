<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Constants\ConfigType;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sys_configs')->upsert([
            [
                'desc' => '服裝',
                'code' => 'CLOTHING',
                'type' => ConfigType::EXPENDITURE_TYPE,
            ],
            [
                'desc' => '伙食',
                'code' => 'FOOD',
                'type' => ConfigType::EXPENDITURE_TYPE,
            ],
            [
                'desc' => '旅遊',
                'code' => 'TRAVEL',
                'type' => ConfigType::EXPENDITURE_TYPE,
            ],
            [
                'desc' => '遊樂',
                'code' => 'GAME',
                'type' => ConfigType::EXPENDITURE_TYPE,
            ],
            [
                'desc' => '其他',
                'code' => 'OTHERS',
                'type' => ConfigType::EXPENDITURE_TYPE,
            ],
            [
                'desc' => '現金',
                'code' => 'CASH',
                'type' => ConfigType::EXPENDITURE_PAYMENT_METHOD,
            ],
            [
                'desc' => '信用卡',
                'code' => 'CREDIT',
                'type' => ConfigType::EXPENDITURE_PAYMENT_METHOD,
            ],
        ], [ 'type', 'code' ]);
    }
}
